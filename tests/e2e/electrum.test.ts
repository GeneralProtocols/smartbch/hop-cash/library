import { createElectrumCluster, getCurrentHeight, getTransactionHeight, getUnspentOutputs, getTransaction, getTransactionVerbose, getDoubleSpendProof, getDoubleSpendProofScore, broadcastTransaction, getBalance, getHistory } from '../../lib';

// Import library to communicate with the Bitcoin Cash chain/network.
import { ElectrumCluster } from 'electrum-cash';

// Electrum Servers we will use.
const cashProviderUrls: Array<string> =
[
	'fulcrum.generalprotocols.com',
];

// Declare variable for our Electrum Cluster.
let cashProvider: ElectrumCluster;

// Wrap the testing in an async function to enable async/await syntax.
describe('electrum.ts Tests', () =>
{
	describe('createElectrumCluster()', () =>
	{
		test('Should create and connect to Electrum Server', async () =>
		{
			cashProvider = await createElectrumCluster(cashProviderUrls);

			expect(cashProvider).toBeInstanceOf(ElectrumCluster);
		});
	});

	describe('getCurrentHeight()', () =>
	{
		test('Should fetch current block height as Number', async () =>
		{
			const currentHeight = await getCurrentHeight(cashProvider);
			const expectedResponseType = expect.any(Number);

			expect(currentHeight).toStrictEqual(expectedResponseType);
		});
	});

	describe('getTransactionHeight()', () =>
	{
		// TODO: Try to create test-case for transaction that is still in mempool.
		//       Note the "height" returned in this case should equal zero.

		test('Should fetch height of confirmed transaction as Number', async () =>
		{
			const transactionHash = '28ffdb68d9b6973cbe0e07f1a7035503afee508e280ad57093e46bfe43d4653a';
			const expectedHeight = 729489;

			const transactionHeight = await getTransactionHeight(cashProvider, transactionHash);

			expect(transactionHeight).toStrictEqual(expectedHeight);
		});

		test('Should throw Error for transaction that is unknown', async () =>
		{
			const transactionHash = 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';

			// Expect an error for transaction that is unknown.
			expect(
				getTransactionHeight(cashProvider, transactionHash),
			).rejects.toThrowError();
		});
	});

	describe('getUnspentOutputs()', () =>
	{
		test('Should return Array Type given a valid CashAddr', async () =>
		{
			const address = 'bitcoincash:qpppdzk8a7hazup2rkrce7j87wlvkxn7tqmnzgg7f9';
			const expectedResponseType = expect.any(Array);

			const unspentOutputs = await getUnspentOutputs(cashProvider, address);

			expect(unspentOutputs).toStrictEqual(expectedResponseType);
		});

		test('Should return Array Type given a valid Legacy (Base58) Address', async () =>
		{
			const address = '172Sbv2hKnCA1fjsc45pSWYCUbEwc1PvAT';
			const expectedResponseType = expect.any(Array);

			const unspentOutputs = await getUnspentOutputs(cashProvider, address);

			expect(unspentOutputs).toStrictEqual(expectedResponseType);
		});

		test('Should throw Error if invalid CashAddr', async () =>
		{
			const address = 'bitcoincash:qpppdzk8a7hazup2rkrce7j87wlvkxn7tqmnaaaaaa';

			// Expect an error for invalid CashAddr.
			expect(
				getUnspentOutputs(cashProvider, address),
			).rejects.toThrowError();
		});

		test('Should throw Error if invalid Legacy (Base58) Address', async () =>
		{
			const address = '172Sbv2hKnCA1fjsc45pSWYCUbEwAAAAAA';

			// Expect an error for invalid Legacy Address.
			expect(
				getUnspentOutputs(cashProvider, address),
			).rejects.toThrowError();
		});
	});

	describe('getTransaction()', () =>
	{
		test('Should fetch raw hex representation of transaction', async () =>
		{
			const transactionHash = '28ffdb68d9b6973cbe0e07f1a7035503afee508e280ad57093e46bfe43d4653a';
			const expectedTransactionHex = '02000000014861c4e9f10b1b54d6974671de4fa4d6b804fdc35aed60dd112dd482b9d994730100000064418693654d91a984bb1e29bea9bc45b020601bef86034f26a9e7b60d3314605d056b9dba04620b61ea77cf022a9af273fe98e02c3014998ca60957db00a88d5f22412103a5271c8cbffdeb139bba66dc064e5c8095ef5f0b9f5a9381a286a8150061ac4b000000000200000000000000002c6a2a30784545313234613761326263336565643831336265383639634338333037303939333666374336463072e21600000000001976a91452ffca2f69897fc91a416376761d4bd6d7adcf7688ac00000000';

			const transactionHex = await getTransaction(cashProvider, transactionHash);

			expect(transactionHex).toStrictEqual(expectedTransactionHex);
		});

		test('Should throw Error for transaction that is unknown', async () =>
		{
			const transactionHash = 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';

			// Expect an error for transaction that is unknown.
			expect(
				getTransaction(cashProvider, transactionHash),
			).rejects.toThrowError();
		});
	});

	describe('getTransactionVerbose()', () =>
	{
		test('Should return expected structure for transaction', async () =>
		{
			const transactionHash = '28ffdb68d9b6973cbe0e07f1a7035503afee508e280ad57093e46bfe43d4653a';
			const expectedTransactionStructure = expect.objectContaining({
				blockhash: expect.any(String),
				blocktime: expect.any(Number),
				confirmations: expect.any(Number),
				hash: expect.any(String),
				hex: expect.any(String),
				locktime: expect.any(Number),
				size: expect.any(Number),
				time: expect.any(Number),
				txid: expect.any(String),
				version: expect.any(Number),
				vin: expect.any(Array),
				vout: expect.any(Array),
			});

			const transactionObject = await getTransactionVerbose(cashProvider, transactionHash);

			expect(transactionObject).toMatchObject(expectedTransactionStructure);
		});

		test('Should throw Error for transaction that is unknown', async () =>
		{
			const transactionHash = 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';

			// Expect an error for transaction that is unknown.
			expect(
				getTransactionVerbose(cashProvider, transactionHash),
			).rejects.toThrowError();
		});
	});

	describe('getDoubleSpendProof()', () =>
	{
		// TODO: Test for transaction that is in mempool.
		//       This is going to be very difficult to do reliably.
		//       Even if we broadcast a transaction, there is possibility of block being mined before this call is made.

		test('Should return null if transaction not in mempool', async () =>
		{
			const transactionHash = '28ffdb68d9b6973cbe0e07f1a7035503afee508e280ad57093e46bfe43d4653a';
			const expectedResponse = null;

			const response = await getDoubleSpendProof(cashProvider, transactionHash);

			expect(response).toStrictEqual(expectedResponse);
		});
	});

	describe('getDoubleSpendProofScore()', () =>
	{
		// TODO: Test for transaction that is in mempool.
		//       This is going to be very difficult to do reliably.
		//       Even if we broadcast a transaction, there is possibility of block being mined before this call is made.

		test('Should throw Error for transaction not in mempool', async () =>
		{
			const transactionHash = '28ffdb68d9b6973cbe0e07f1a7035503afee508e280ad57093e46bfe43d4653a';

			// Expect an error containing the word "mempool" if transaction is not in mempool.
			expect(
				getDoubleSpendProofScore(cashProvider, transactionHash),
			).rejects.toThrowError('mempool');
		});
	});

	describe('broadcastTransaction()', () =>
	{
		// TODO: Test for success case where transaction is valid.
		//       This is going to be difficult to do as we would need a valid wallet.

		test('Should throw Error for transaction where Inputs are already spent', async () =>
		{
			const transactionHex = '0200000002755f36bec2ac62b8b5927aa3350462c44d7b54bcba15b22423b35bcb81606c6a0100000064418f3e0f44b6c3640e102d458439583dce21b3a4651af3b92e2685b1db581a2848c4cc1bd20ee49a78aedda0f7f29725866d9dc5eaeec937ec53e2c352e687c1f4412102b32842980b08141256602e58bacde451f8813a17842b31cafa30172b3eed1d1a000000003de20200aeb760370a17559dd7337a3a200b025e2cea82e92cae9953105829bc010000006441e66544bb012515744345f0a1f850ffa132ed472211988789714d1c977a744da7a61829459edaf5ad906072a06da486e352e175e912b427295f4c36b46ea59145412102b32842980b08141256602e58bacde451f8813a17842b31cafa30172b3eed1d1a00000000030000000000000000226a2071e850a7f136bbf0808d1ec0eed1158063849d8bf9022b871687ef9490e2b1f720a10700000000001976a914b9e9a1408867abc8ceb1fd9ac8aa6236f2a9bb1d88ac8d9f0700000000001976a91457c240ed598abc2fe208409aab6ba327958f8cfd88ac00000000';

			// Expect an error if transaction inputs are already spent.
			expect(
				broadcastTransaction(cashProvider, transactionHex),
			).rejects.toThrowError();
		});
	});

	describe('getBalance()', () =>
	{
		test('Should return Number given a valid CashAddr', async () =>
		{
			const address = 'bitcoincash:qpppdzk8a7hazup2rkrce7j87wlvkxn7tqmnzgg7f9';
			const expectedResponseType = expect.any(Number);

			const balance = await getBalance(cashProvider, address);

			expect(balance).toStrictEqual(expectedResponseType);
		});

		test('Should return Number given a valid Legacy (Base58) Address', async () =>
		{
			const address = '172Sbv2hKnCA1fjsc45pSWYCUbEwc1PvAT';
			const expectedResponseType = expect.any(Number);

			const balance = await getBalance(cashProvider, address);

			expect(balance).toStrictEqual(expectedResponseType);
		});

		test('Should throw Error if invalid CashAddr', async () =>
		{
			const address = 'bitcoincash:qpppdzk8a7hazup2rkrce7j87wlvkxn7tqmnaaaaaa';

			// Expect an error for invalid CashAddr.
			expect(
				getBalance(cashProvider, address),
			).rejects.toThrowError();
		});

		test('Should throw Error if invalid Legacy (Base58) Address', async () =>
		{
			const address = '172Sbv2hKnCA1fjsc45pSWYCUbEwAAAAAA';

			// Expect an error for invalid Legacy Address.
			expect(
				getBalance(cashProvider, address),
			).rejects.toThrowError();
		});
	});

	describe('getHistory()', () =>
	{
		test('Should return Array Type given a valid CashAddr', async () =>
		{
			const address = 'bitcoincash:qpppdzk8a7hazup2rkrce7j87wlvkxn7tqmnzgg7f9';
			const expectedResponseType = expect.any(Array);

			const addressHistory = await getHistory(cashProvider, address);

			expect(addressHistory).toStrictEqual(expectedResponseType);
		});

		test('Should return Array Type given a valid Legacy (Base58) Address', async () =>
		{
			const address = '172Sbv2hKnCA1fjsc45pSWYCUbEwc1PvAT';
			const expectedResponseType = expect.any(Array);

			const addressHistory = await getHistory(cashProvider, address);

			expect(addressHistory).toStrictEqual(expectedResponseType);
		});

		test('Should throw Error if invalid Address given', async () =>
		{
			const address = 'bitcoincash:someInvalidAddress';

			expect(
				getHistory(cashProvider, address),
			).rejects.toThrowError();
		});
	});

	afterAll(async () =>
	{
		// cSpell:ignore TLSWRAP
		// NOTE: Jest detects an open handle despite calling this.
		//       The culprit appears to be related to TLSWRAP and may be a Jest/Node bug.
		//       More information: https://github.com/facebook/jest/issues/11665#issuecomment-961947393
		// TODO: Remove the --forceExit flag from "npm run test:e2e" once fixed.
		await cashProvider.shutdown();
	});
});
