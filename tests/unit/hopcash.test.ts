import {
	// Constants
	CASH_SATOSHI_DUST_LIMIT, CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD, CASH_REQUIRES_TWO_CONFIRMATIONS_THRESHOLD, SATOSHIS_PER_BITCOIN_CASH,

	// Functions
	createCashBridgeTransaction, createCashP2PKHTransaction, createCashPayoutTransaction, parseCashBridgeTransaction, parseCashPayoutTransaction, parseSmartBridgeTransaction, getRequiredCashConfirmations, calculateBridgeFee, deriveLegacyHopWalletPrivateKey, deriveHopWalletPrivateKey,
} from '../../lib';

// Import fixture for createCashBridgeTransaction.
import { successCases as createCashBridgeTransactionSuccessCases } from './fixtures/create-cash-bridge-transaction.json';

// Import fixture for createCashP2PKHTransaction.
import { successCases as createCashP2PKHTransactionSuccessCases } from './fixtures/create-cash-p2pkh-transaction.json';

// Import fixture for createCashPayoutTransaction.
import { successCases as createCashPayoutTransactionSuccessCases } from './fixtures/create-cash-payout-transaction.json';

// Import fixture for parseCashBridgeTransaction.
import { successCases as parseCashBridgeTransactionSuccessCases, failCases as parseCashBridgeTransactionFailCases } from './fixtures/parse-cash-bridge-transaction.json';

// Import fixture for parseCashPayoutTransaction.
import { successCases as parseCashPayoutTransactionSuccessCases } from './fixtures/parse-cash-payout-transaction.json';

// Import fixture for parseSmartBridgeTransaction.
import { successCases as parseSmartBridgeTransactionSuccessCases, failCases as parseSmartBridgeTransactionFailCases } from './fixtures/parse-smart-bridge-transaction.json';

// This fixture was generated using Hop.Cash V1 derivation code.
// It contains 1000 signatures and their corresponding wallet keys that we can use to check against our legacy support implementation.
import deriveLegacyHopWalletPrivateKeyFixture from './fixtures/derive-legacy-hop-wallet-private-key.json';

// This fixture was generated using Hop.Cash V2 derivation code.
// It contains 1000 signatures and their corresponding Private Keys that we can use to ensure derivation is consistent.
import deriveHopWalletPrivateKeyFixture from './fixtures/derive-hop-wallet-private-key.json';

// Import binToHex and hexToBin from LibAuth.
import { binToHex, hexToBin } from '@bitauth/libauth';

// Wrap the testing in an async function to enable async/await syntax.
describe('hopcash.ts Tests', () =>
{
	describe('createCashBridgeTransaction()', () =>
	{
		// Check that our createBridgeTransaction creates a valid Cash Transaction Structure.
		test.each(createCashBridgeTransactionSuccessCases)('Should create a valid transaction structure $#',
			async ({ privateKeyWIF, unspentOutputs, bridgeAddress, payoutAddress, expectedTransactionWithoutFee }) =>
			{
				// Run createCashBridgeTransaction on the input...
				const result = await createCashBridgeTransaction(privateKeyWIF, unspentOutputs, bridgeAddress, payoutAddress);

				// And make sure it matches the expected output.
				expect(result).toStrictEqual(hexToBin(expectedTransactionWithoutFee));
			});

		// Check that the transaction fee is subtracted from the value output.
		test.each(createCashBridgeTransactionSuccessCases)('Should subtract fee from value output $#',
			async ({ privateKeyWIF, unspentOutputs, bridgeAddress, payoutAddress, expectedTransactionWithFee }) =>
			{
				// Run createCashBridgeTransaction on the input to get initial fee estimate...
				const transactionEstimate = await createCashBridgeTransaction(privateKeyWIF, unspentOutputs, bridgeAddress, payoutAddress);

				// Run it again and specify a fee based on the length of the transaction (one satoshi per byte).
				const result = await createCashBridgeTransaction(privateKeyWIF, unspentOutputs, bridgeAddress, payoutAddress, transactionEstimate.length);

				// And make sure it matches the expected output.
				// NOTE: The fee is deducted from the value output in the expected result.
				expect(result).toStrictEqual(hexToBin(expectedTransactionWithFee));
			});

		test.each(createCashBridgeTransactionSuccessCases)('Should parse with parseCashBridgeTransaction $#',
			async ({ privateKeyWIF, unspentOutputs, bridgeAddress, payoutAddress, expectedParseCashBridgeTransaction }) =>
			{
				// Run createCashBridgeTransaction on the input...
				const transaction = await createCashBridgeTransaction(privateKeyWIF, unspentOutputs, bridgeAddress, payoutAddress);

				// Parse it with parseCashBridgeTransaction.
				const result = await parseCashBridgeTransaction(binToHex(transaction));

				// And make sure it matches the expected output.
				expect(result).toMatchObject(expectedParseCashBridgeTransaction);
			});

		// TODO: It would be ideal to have some fail cases here too.
		//       1. Invalid inputs (e.g. Invalid Payout Address).
		//       2. Insufficient balance.
	});

	describe('createCashPayoutTransaction()', () =>
	{
		// Check that our createCashPayoutTransaction creates a valid Cash Transaction Structure.
		test.each(createCashPayoutTransactionSuccessCases)('Should create a valid transaction structure $#',
			async ({ privateKeyWIF, unspentOutputs, payoutAddress, sourceTransaction, expectedTransactionWithoutFee }) =>
			{
				// Calculate total balance available in unspent outputs.
				const balance = unspentOutputs.reduce((total, unspentOutput) => total + unspentOutput.value, 0);

				// Send half of that balance in our payout transaction.
				const satoshis = balance * 0.5;

				// Run createCashPayoutTransaction on the input...
				const result = await createCashPayoutTransaction(privateKeyWIF, unspentOutputs, payoutAddress, satoshis, sourceTransaction);

				// And make sure it matches the expected output.
				expect(result).toStrictEqual(hexToBin(expectedTransactionWithoutFee));
			});

		test.each(createCashPayoutTransactionSuccessCases)('Should subtract fee from change output $#',
			async ({ privateKeyWIF, unspentOutputs, payoutAddress, sourceTransaction, expectedTransactionWithFee }) =>
			{
				// Calculate total balance available in unspent outputs.
				const balance = unspentOutputs.reduce((total, unspentOutput) => total + unspentOutput.value, 0);

				// Send half of that balance in our payout transaction.
				const satoshis = balance * 0.5;

				// Run createCashPayoutTransaction on the input to get initial fee estimate...
				const transactionEstimate = await createCashPayoutTransaction(privateKeyWIF, unspentOutputs, payoutAddress, satoshis, sourceTransaction);

				// Run it again and specify a fee based on the length of the transaction (one satoshi per byte).
				const result = await createCashPayoutTransaction(privateKeyWIF, unspentOutputs, payoutAddress, satoshis, sourceTransaction, transactionEstimate.length);

				// And make sure it matches the expected output.
				// NOTE: The fee is deducted from the change output in the expected result.
				expect(result).toStrictEqual(hexToBin(expectedTransactionWithFee));
			});

		test.each(createCashPayoutTransactionSuccessCases)('Should omit change output if below dust limit $#',
			async ({ privateKeyWIF, unspentOutputs, payoutAddress, sourceTransaction, expectedTransactionWithoutChangeOutput }) =>
			{
				// Calculate total balance available in unspent outputs.
				const balance = unspentOutputs.reduce((total, unspentOutput) => total + unspentOutput.value, 0);

				// Send balance minus the dust limit so that there is not enough for a change output.
				const satoshis = balance - CASH_SATOSHI_DUST_LIMIT;

				// Run parseCashBridgeTransaction on the input...
				const transactionEstimate = await createCashPayoutTransaction(privateKeyWIF, unspentOutputs, payoutAddress, satoshis, sourceTransaction);

				// Run it again and specify a fee based on the length of the transaction (one satoshi per byte).
				const result = await createCashPayoutTransaction(privateKeyWIF, unspentOutputs, payoutAddress, satoshis, sourceTransaction, transactionEstimate.length);

				// And make sure it matches the expected output.
				// NOTE: The change output is omitted in the expected result.
				expect(result).toStrictEqual(hexToBin(expectedTransactionWithoutChangeOutput));
			});

		test.each(createCashPayoutTransactionSuccessCases)('Should parse with parseCashBridgeTransaction $#',
			async ({ privateKeyWIF, unspentOutputs, payoutAddress, sourceTransaction, expectedParseCashPayoutTransaction }) =>
			{
				// Calculate total balance available in unspent outputs.
				const balance = unspentOutputs.reduce((total, unspentOutput) => total + unspentOutput.value, 0);

				// Send half of that balance in our payout transaction.
				const satoshis = balance * 0.5;

				// Run createCashPayoutTransaction on the input...
				const transaction = await createCashPayoutTransaction(privateKeyWIF, unspentOutputs, payoutAddress, satoshis, sourceTransaction);

				// Parse the Payout Transaction.
				const result = await parseCashPayoutTransaction(binToHex(transaction));

				// And make sure it matches the expected output.
				expect(result).toMatchObject(expectedParseCashPayoutTransaction);
			});

		// TODO: It would be ideal to have some fail cases here too.
		//       1. Invalid inputs (e.g. Invalid Payout Address and Source Transaction).
		//       2. Insufficient balance (this would require refactor of Hop.Cash backend first).
	});

	describe('createCashP2PKHTransaction()', () =>
	{
		test.each(createCashP2PKHTransactionSuccessCases)('Should create a valid transaction structure $#',
			async ({ privateKeyWIF, unspentOutputs, targetAddress, expectedTransactionWithoutFee }) =>
			{
				// Run parseCashBridgeTransaction on the input...
				const result = await createCashP2PKHTransaction(privateKeyWIF, unspentOutputs, targetAddress, 500000);

				// And make sure it matches the expected output.
				expect(result).toStrictEqual(hexToBin(expectedTransactionWithoutFee));
			});

		test.each(createCashP2PKHTransactionSuccessCases)('Should subtract fee from change output $#',
			async ({ privateKeyWIF, unspentOutputs, targetAddress, expectedTransactionWithFee }) =>
			{
				// Run createCashP2PKHTransaction with the given test data...
				const transactionEstimate = await createCashP2PKHTransaction(privateKeyWIF, unspentOutputs, targetAddress, 500000);

				// Run it again and specify a fee based on the length of the transaction (one satoshi per byte).
				const result = await createCashP2PKHTransaction(privateKeyWIF, unspentOutputs, targetAddress, 500000, transactionEstimate.length);

				// And make sure it matches the expected output.
				expect(result).toStrictEqual(hexToBin(expectedTransactionWithFee));
			});

		test.each(createCashP2PKHTransactionSuccessCases)('Should omit change output if below dust limit $#',
			async ({ privateKeyWIF, unspentOutputs, targetAddress, expectedTransactionWithoutChangeOutput }) =>
			{
				// Run createCashP2PKHTransaction with the given test data...
				const transactionEstimate = await createCashP2PKHTransaction(privateKeyWIF, unspentOutputs, targetAddress, 1000000 - CASH_SATOSHI_DUST_LIMIT);

				// Run it again and specify a fee based on the length of the transaction (one satoshi per byte).
				const result = await createCashP2PKHTransaction(privateKeyWIF, unspentOutputs, targetAddress, 1000000 - CASH_SATOSHI_DUST_LIMIT, transactionEstimate.length);

				// And make sure it matches the expected output.
				expect(result).toStrictEqual(hexToBin(expectedTransactionWithoutChangeOutput));
			});

		// TODO: It would be ideal to have some fail cases here too.
		//       1. Invalid inputs (e.g. Invalid Target Address).
	});

	describe('parseCashBridgeTransaction()', () =>
	{
		test.each(parseCashBridgeTransactionSuccessCases)('Should parse valid Cash Bridge Transaction $#',
			async ({ transactionHex, expectedOutput }) =>
			{
				// Run parseCashBridgeTransaction on the transaction...
				const result = await parseCashBridgeTransaction(transactionHex);

				// And make sure it matches the expected output.
				expect(result).toMatchObject(expectedOutput);
			});

		test('Should throw an error if payout address is not Smart Address', async () =>
		{
			// Expect an error containing the word "Address" if invalid Smart Address given.
			expect(parseCashBridgeTransaction(parseCashBridgeTransactionFailCases.invalidPayoutAddress)).rejects.toThrowError('Address');
		});

		test('Should throw an error if payout address is not present (no OP_RETURN).', async () =>
		{
			// Expect an error containing the word "OP_RETURN" if transaction's first output is not an OP_RETURN.
			expect(parseCashBridgeTransaction(parseCashBridgeTransactionFailCases.noOpReturn)).rejects.toThrowError('OP_RETURN');
		});
	});

	// Run test cases that verify failure behaviors.
	describe('parseCashPayoutTransaction()', () =>
	{
		test.each(parseCashPayoutTransactionSuccessCases)('Should parse valid Cash Payout Transaction $#',
			async ({ transactionHex, expectedOutput }) =>
			{
				// Run parseCashPayoutTransaction on the transaction...
				const result = await parseCashPayoutTransaction(transactionHex);

				// And make sure it matches the expected output.
				expect(result).toMatchObject(expectedOutput);
			});
	});

	describe('parseSmartBridgeTransaction()', () =>
	{
		test.each(parseSmartBridgeTransactionSuccessCases)('Should parse valid Smart Bridge Transaction $#',
			async ({ transaction, expectedOutput }) =>
			{
				// Run parseSmartBridgeTransaction on the transaction...
				// NOTE: Our fixture is a JSON.stringified transaction so we have to pass in as 'any' here.
				const result = await parseSmartBridgeTransaction(transaction as any);

				// And make sure it matches the expected output.
				expect(result).toMatchObject(expectedOutput);
			});

		test('Should throw an error if payout address is not a valid Cash Address.', async () =>
		{
			// Expect an error containing the term "Cash Address" if the payout address is not a valid Cash Address.
			expect(
				// NOTE: Our fixture is a JSON.stringified transaction so we have to pass in as 'any' here.
				parseSmartBridgeTransaction(parseSmartBridgeTransactionFailCases.invalidPayoutAddress as any),
			).rejects.toThrowError('Cash Address');
		});

		test('Should throw an error if data string is empty.', async () =>
		{
			// Expect an error containing the term "Cash Address" if "data" in the transaction is empty.
			expect(
				// NOTE: Our fixture is a JSON.stringified transaction so we have to pass in as 'any' here.
				parseSmartBridgeTransaction(parseSmartBridgeTransactionFailCases.emptyData as any),
			).rejects.toThrowError('Cash Address');
		});
	});

	describe('getRequiredCashConfirmations()', () =>
	{
		test(`Should return 0 if less than ${CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD} BCH.`, async () =>
		{
			// Set amount to one confirmation threshold.
			const amount = (CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD * SATOSHIS_PER_BITCOIN_CASH);

			// Get the required number of confirmations for this amount.
			const result = await getRequiredCashConfirmations(amount);

			// And make sure it equals zero.
			expect(result).toStrictEqual(0);
		});

		test(`Should return 1 if more than ${CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD} BCH and less than ${CASH_REQUIRES_TWO_CONFIRMATIONS_THRESHOLD} BCH.`, async () =>
		{
			// Set amount to one confirmation threshold plus 1 satoshi.
			const amount = (CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD * SATOSHIS_PER_BITCOIN_CASH) + 1;

			// Get the required number of confirmations for this amount.
			const result = await getRequiredCashConfirmations(amount);

			// And make sure it equals 1.
			expect(result).toStrictEqual(1);
		});

		test(`Should return 2 if more than ${CASH_REQUIRES_TWO_CONFIRMATIONS_THRESHOLD} BCH.`, async () =>
		{
			// Set amount to two confirmation threshold plus 1 satoshi.
			const amount = (CASH_REQUIRES_TWO_CONFIRMATIONS_THRESHOLD * SATOSHIS_PER_BITCOIN_CASH) + 1;

			// Get the required number of confirmations for this amount.
			const result = await getRequiredCashConfirmations(amount);

			// And make sure it equals 2.
			expect(result).toStrictEqual(2);
		});
	});

	describe('calculateBridgeFee()', () =>
	{
		test('Should return 0.1% of amount.', async () =>
		{
			// Set amount to 100,000,000 satoshis (1 BCH).
			const satoshis = 100_000_000;

			// Expect a fee of 100,000 satoshis (0.1% of 100,000,000).
			const expected = 100_000;

			// Calculate the bridge fee for our Satoshi amount.
			const result = await calculateBridgeFee(satoshis);

			// And make sure it equals our expected fee.
			expect(result).toStrictEqual(expected);
		});

		test('Should never return less than the minimum fee.', async () =>
		{
			// Set amount to 5,000,000 satoshis (0.005 BCH).
			const satoshis = 5_000_000;

			// Expect a fee of 10,000 satoshis.
			// NOTE: 0.1% of 5,000,000 is 5,000 (less than our minimum which is 10,000).
			const expected = 10_000;

			// Calculate the bridge fee for our Satoshi amount.
			const result = await calculateBridgeFee(satoshis);

			// And make sure it equals our expected fee.
			expect(result).toStrictEqual(expected);
		});
	});

	describe('deriveLegacyHopWalletPrivateKey()', () =>
	{
		test(`Should derive the same Private Keys as Hop Cash V1 (Testing ${deriveLegacyHopWalletPrivateKeyFixture.length}).`, async () =>
		{
			// Iterate through each entry in our fixture.
			// NOTE: We could use test.each, but this would clutter our Jest output and overshadow other tests.
			for(const versionOneDerivation of deriveLegacyHopWalletPrivateKeyFixture)
			{
				// Derive wallet using Version 2 derivation.
				const generatedPrivateKey = await deriveLegacyHopWalletPrivateKey(versionOneDerivation.signature);

				// Make sure the Private Key generated is 32 bytes.
				expect(generatedPrivateKey.length).toStrictEqual(32);

				// Make sure the Private Key generated matches Hop Wallet Version 1 derivation.
				expect(binToHex(generatedPrivateKey)).toStrictEqual(versionOneDerivation.privateKey);
			}
		});
	});

	describe('deriveHopWalletPrivateKey()', () =>
	{
		test(`Should derive consistent Private Keys given a Signature and Hop Wallet Prime (Testing ${deriveHopWalletPrivateKeyFixture.length}).`, async () =>
		{
			// Iterate through each entry in our fixture.
			// NOTE: We could use test.each, but this would clutter our Jest output and overshadow other tests.
			for(const precomputedPrivateKey of deriveHopWalletPrivateKeyFixture)
			{
				// Derive wallet using Version 2 derivation.
				const generatedPrivateKey = await deriveHopWalletPrivateKey(precomputedPrivateKey.signature);

				// Make sure the Private Key generated is 32 bytes.
				expect(generatedPrivateKey.length).toStrictEqual(32);

				// Make sure the Private Key generated matches the precomputed Private Key.
				expect(binToHex(generatedPrivateKey)).toStrictEqual(precomputedPrivateKey.privateKey);
			}
		});
	});
});
