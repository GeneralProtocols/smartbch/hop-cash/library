# Hop.Cash Library

Hop.Cash Library for shared Frontend and Backend code.

## Installation

Installing the library.

```sh
npm install @generalprotocols/hop-cash
```

## Usage

Before you can use the Hop.Cash Library functionality in your project, you need to import it into your project:

```js
// Import the library.
import { ... } from '@generalprotocols/hop-cash';
```

Note that although the documentation lists functionality in modules (for clarity), all exports are at the global level to better allow for Tree-Shaking (Dead Code Elimination).

### Parse Cash Bridge Transaction

This is a transaction from the user's front end temporary Cash wallet to the backend receiving Cash wallet with an OP_RETURN to indicate the user's desired Smart Payout Address.

```
import { getTransaction, parseCashBridgeTransaction } from '@generalprotocols/hop-cash';

// Fetch transaction given a transaction hash.
const transactionHex = await getTransaction(electrumClientOrCluster, transactionHash);

// Parse it with parseCashBridgeTransaction...
const parsedCashBridgeTransaction = await parseCashBridgeTransaction(transactionHex);

// Example Response
// {
//   "transactionHash": "31abb7ecf3acd99cc2c4504692979e9f919b35d1408db77ec47c1182b6a3b4d1",
//   "payoutAddress": "0xEE124a7a2bc3eed813be869cC830709936f7C6F0",
//   "bridgeAddress": "bitcoincash:qpf0lj30dxyhljg6g93hvasaf0td0tw0wcy4u40s7w",
//   "bridgeSatoshis": 1499762
// }
```

### Parse Smart Bridge Transaction

This is a transaction from the user's Smart wallet to the backend receiving Smart wallet with data attached to indicate the user's desired Cash Payout Address.

```
import { parseSmartBridgeTransaction } from '@generalprotocols/hop-cash';
import { ethers } from 'ethers';

// Get transaction using an Ethers Smart Provider.
const transaction = await smartProvider.getTransaction(transactionHash);

// Parse it with parseSmartBridgeTransaction...
const parsedSmartBridgeTransaction = await parseSmartBridgeTransaction(transaction);

// Example Response
// {
//   "transactionHash": "0x185b933a552d8f0b606d32c0e74b8b052c3df864f1c4c592868749fcb31f9d13",
//   "bridgeAddress": "0x3207d65b4D45CF617253467625AF6C1b687F720b",
//   "bridgeSatoshis": 40000000,
//   "payoutAddress": "bitcoincash:qz9jy5a4hd73gsgweur7pc8n3sa48keqavdcutq37c"
// }
```

### Parse Cash Payout Transaction

This is a transaction from the Hop.Cash Outgoing/Liquidity wallet to the user's Cash Payout Address with an OP_RETURN to indicate the original Smart Source Transaction.

```
import { getTransaction, parseCashBridgeTransaction } from '@generalprotocols/hop-cash';

// Fetch transaction given a transaction hash.
const transactionHex = await getTransaction(electrumClientOrCluster, transactionHash);

// Parse it with parseCashPayoutTransaction...
const parsedCashBridgeTransaction = await parseCashPayoutTransaction(transactionHex);

// Example Response
// {
//   "transactionHash": "9eb7448b95bab2bc38d8b3c2ce63cff1f62761eff505bde2ab68cf5d7859aa50",
//   "sourceTransaction": "0x6f1df436ead92c230e015809268285216dcbe09cc4990d03d17098169d06782c",
//   "payoutAddress": "bitcoincash:qrm5f4feeee6wf4w4ckcw07vh5ldtdn2ty3gvy7f0k",
//   "payoutSatoshis": 1490000
// }
```

### Create Cash Bridge Transaction

This is a transaction from the user's front end temporary Cash wallet to the backend receiving Cash wallet with an OP_RETURN to indicate the user's desired Smart Payout Address.

```
import { createCashBridgeTransaction, getUnspentOutputs } from '@generalprotocols/hop-cash';

// Get unspent outputs for address.
const unspentOutputs = await getUnspentOutputs(electrumClientOrCluster, cashFrontendWalletAddress);
// Example Response
// [
//   {
//     "height": 0,
//     "tx_hash": "6a6c6081cb5bb32324b215babc547b4dc4620435a37a92b5b862acc2be365f75",
//     "tx_pos": 1,
//     "value": 500000
//   }
// ]

// Run createCashBridgeTransaction to get initial fee estimate...
const transactionEstimate = await createCashBridgeTransaction(privateKeyWIF, unspentOutputs, bridgeAddress, payoutAddress);

// Run it again and specify a fee based on the length of the transaction (one satoshi per byte).
const transaction = await createCashBridgeTransaction(privateKeyWIF, unspentOutputs, bridgeAddress, payoutAddress, transactionEstimate.length);
```

### Create Cash Payout Transaction

This is a transaction from the Hop.Cash Outgoing/Liquidity wallet to the user's Cash Payout Address with an OP_RETURN to indicate the original Smart Source Transaction.

```
import { createCashPayoutTransaction } from '@generalprotocols/hop-cash';

// Get unspent outputs for address.
const unspentOutputs = await getUnspentOutputs(electrumClientOrCluster, cashLiquidityAddress);
// Example Response
// [
//   {
//     "height": 0,
//     "tx_hash": "6a6c6081cb5bb32324b215babc547b4dc4620435a37a92b5b862acc2be365f75",
//     "tx_pos": 1,
//     "value": 500000
//   }
// ]

// Run createCashPayoutTransaction on the input to get initial fee estimate...
const transactionEstimate = await createCashPayoutTransaction(privateKeyWIF, unspentOutputs, payoutAddress, satoshis, sourceTransaction);

// Run it again and specify a fee based on the length of the transaction (one satoshi per byte).
const result = await createCashPayoutTransaction(privateKeyWIF, unspentOutputs, payoutAddress, satoshis, sourceTransaction, transactionEstimate.length);
```

### Create Cash P2PKH Transaction

This is a generic P2PKH transaction for internal Hop.Cash use (e.g. Transferring Liquidity Balancer funds, setting up Wallets for Testing, etc).

```
import { createCashP2PKHTransaction } from '@generalprotocols/hop-cash';

// Get unspent outputs for address.
const unspentOutputs = await getUnspentOutputs(electrumClientOrCluster, fromAddress);
// Example Response
// [
//   {
//     "height": 0,
//     "tx_hash": "6a6c6081cb5bb32324b215babc547b4dc4620435a37a92b5b862acc2be365f75",
//     "tx_pos": 1,
//     "value": 500000
//   }
// ]

// Run createCashP2PKHTransaction with the given input...
const transactionEstimate = await createCashP2PKHTransaction(privateKeyWIF, unspentOutputs, targetAddress, 500000);

// Run it again and specify a fee based on the length of the transaction (one satoshi per byte).
const result = await createCashP2PKHTransaction(privateKeyWIF, unspentOutputs, targetAddress, 500000, transactionEstimate.length);
```

## Documentation

For a complete list of constants, methods and parameters, read the [API documentation](https://generalprotocols.gitlab.io/smartbch/hop-cash/library/).
