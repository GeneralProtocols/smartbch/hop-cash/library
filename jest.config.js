module.exports =  {
	// Allow Jest to natively transpile TS files
	testEnvironment: 'node',
	preset: 'ts-jest',
	transformIgnorePatterns: [ '<rootDir>/node_modules/' ],

	// Configure reporters
	reporters: [ 'default', 'jest-junit' ],
	testResultsProcessor: 'jest-junit',

	// Only include lib folder when checking code coverage
	collectCoverageFrom: [
		'lib/**/*.ts',
	],
	coverageReporters: [ 'text', 'lcov' ],

	// Configure Unit Tests and E2E tests
	projects: [
		{
			preset: 'ts-jest',
			displayName: 'unit',
			testMatch: [ '<rootDir>/tests/unit/*.(spec|test).ts' ],
		},
		{
			preset: 'ts-jest',
			displayName: 'e2e',
			testMatch: [ '<rootDir>/tests/e2e/*.(spec|test).ts' ],
		},
	],
};
