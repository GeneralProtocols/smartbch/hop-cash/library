// Import package.json to reduce name repetition.
// eslint-disable-next-line import/extensions
import pkg from './package.json';

// Import plugin-dts so that we can roll-up our .d.ts definition files into one file.
import dts from 'rollup-plugin-dts';

export default
[
	// Build main module in both ES and CJS
	{
		// Specify entry point for building
		input: 'dist/module/lib/index.js',

		// Omit all dependencies and peer dependencies from the final bundle
		external: [
			...Object.keys(pkg.dependencies || {}),
			...Object.keys(pkg.peerDependencies || {}),
		],

		// Produce outputs for ES and CJS
		output: [
			{ file: pkg.module, format: 'es' },
			{ file: pkg.main, format: 'cjs' },
		],
	},
	// Build type definitions for the library
	{
		input: 'dist/module/lib/index.d.ts',
		output: [{ file: pkg.typings, format: 'es' }],
		plugins: [ dts() ],
	},
];
