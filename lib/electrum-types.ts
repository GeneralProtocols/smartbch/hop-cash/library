/**
* @module ElectrumTypes
* @description Types describing responses from the Electrum Servers.
*/

// TODO: Ideally, all of these types would belong to an ElectrumTypes category when documentation is generated.
//       However, better-docs does not currently support this (tested V2.3.2 and V2.7.2).
//       Thus, these types will, unfortunately, appear under Global.

// TODO: The functionality in this file pertains to the Bitcoin Cash Network.
//       In future, once a GP Wallet Library is available, we should ideally leverage that.

/** Headers Subscribe Electrum Response. */
export type HeadersSubscribeResponse =
{
	height: number;
	hex: string;
};

/**
* Address List Unspent Entry.
*/
export type AddressListUnspentEntry =
{
	tx_pos: number;     // eslint-disable-line camelcase
	tx_hash: string;    // eslint-disable-line camelcase
	height: number;
	value: number;
};

/** Address List Unspent Response */
export type AddressListUnspentResponse = AddressListUnspentEntry[];

/** Address Get History Entry */
export type AddressGetHistoryEntry =
{
	height: number;
	tx_hash: string;    // eslint-disable-line camelcase
};

/** Address Get History Response */
export type AddressGetHistoryResponse = Array<AddressGetHistoryEntry>;

/** Transaction Double Spend Proof Entry */
export type TransactionDsProofEntryGet =
{
	dspid: string;
	txid: string;
	hex: string;
	outpoint:
	{
		txid: string;
		vout: number;
	};
	descendants: string[];
};

/** Transaction Double Spend Proof Response */
export type TransactionDsProofGetResponse = null | TransactionDsProofEntryGet;

/** This is the most important subset of the actual returned data */
export type ElectrumTransactionInput =
{
	scriptSig:
	{
		asm: string;
		hex: string;
	};
	sequence: number;
	txid: string;
	vout: number;
};

/** Electrum Transaction Output. */
export type ElectrumTransactionOutput =
{
	n: number;
	scriptPubKey:
	{
		addresses: string[];
		asm: string;
		hex: string;
		type: string;
	};
	value: number;
};

/** Electrum Transaction Verbose Response. */
export type TransactionGetVerboseResponse =
{
	vin: ElectrumTransactionInput[];
	vout: ElectrumTransactionOutput[];
	version: number;
	locktime: number;
	txid: string;
	hex: string;
	confirmations: number;
	blocktime?: number;
	blockhash?: string;
};

/** Electrum Transaction. */
export type ElectrumTransaction = TransactionGetVerboseResponse;

/** Electrum Transaction Response */
export type TransactionGetResponse = string;

/** Transaction Get Height Unknown Response */
export type TransactionGetHeightUnknown = null;

/** Transaction Get Height Unconfirmed Response */
export type TransactionGetHeightUnconfirmed = 0;

/** Electrum Transaction Get Height Response */
export type TransactionGetHeightResponse = number | TransactionGetHeightUnconfirmed | TransactionGetHeightUnknown;

/** Transaction Broadcast Response */
export type TransactionBroadcastResponse = string;

/** Address Get Balance Response */
export type AddressGetBalanceResponse =
{
	confirmed: number;
	unconfirmed: number;
};

/**
 * To be used for when an internal (typically connection-related) or data error occurs within Electrum.
 */
export class ElectrumOrDataError extends Error
{
	constructor(message: string)
	{
		super();
		this.message = message;
	}
}
