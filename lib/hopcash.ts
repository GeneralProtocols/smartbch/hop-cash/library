/**
* @module HopCash
* @description Hop.Cash Transaction Parsing and Creation.
*/

// Import the necessary constants.
import { CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD, CASH_REQUIRES_TWO_CONFIRMATIONS_THRESHOLD, CASH_SATOSHI_DUST_LIMIT, FEE_RATE_MULTIPLIER, HOP_WALLET_PRIME, MINIMUM_FEE_SATOSHIS, SATOSHI_TO_WEI_DECIMALS, SATOSHIS_PER_BITCOIN_CASH } from './constants';

// Import the necessary Electrum Types.
import { AddressListUnspentResponse } from './electrum-types';

// Import the necessary interfaces.
import type { ParsedBridgeTransaction, ParsedCashPayoutTransaction } from './interfaces';

// Import tools to work with transaction parsing and generation for bitcoin cash.
import { createValueOutput, createDataOutput, createTransaction, getAddressFromLockingBytecode, getDataFromOutput, getTransactionHash, parseWIF } from './utils';

// Import utilities to convert to and from hex encoded strings.
import { binToBigIntUint64LE, binToBigIntUint256BE, bigIntToBinUint256BEClamped, binToHex, decodeBase58Address, decodeCashAddress, decodeTransactionUnsafe, hexToBin, instantiateSha256, isHex } from '@bitauth/libauth';

// Import library to work with ethereum style addresses.
import { ethers } from 'ethers';

// Set up a text encoder (needed for encoding binary to UTF8).
const textEncoder = new TextEncoder();

// Set up the text decoder (needed for decoding binary from UTF8).
const textDecoder = new TextDecoder();

/**
* Convert satoshi amount to Cash Units (BCH).
*
* If invalid Satoshi amount given, output might not be a valid BCH value.
*
* @function
*
* @param satoshis  {number}  Satoshi Amount to convert to BCH Units.
*
* @returns {number}  Amount in Cash (BCH) Units.
*/
export const naiveSatoshisToCash = function(satoshis: number): number
{
	return satoshis / SATOSHIS_PER_BITCOIN_CASH;
};

/**
* Convert BCH amount to Satoshi Units.
*
* If invalid BCH amount given, output might not be a valid satoshi value.
*
* @function
*
* @param cash  {number}  Cash (BCH) Amount to convert to Satoshi Units.
*
* @returns {number}  Amount in Satoshi Units
*/
export const naiveCashToSatoshis = function(cash: number): number
{
	return cash * SATOSHIS_PER_BITCOIN_CASH;
};

/**
* Display Satoshis in Cash (BCH) Units.
*
* This function is lossy by-design and must not be used to convert units.
*
* @function
*
* @param satoshis       {number}  Satoshi amount to show in BCH Units.
* @param decimalPlaces  {number}  The number of decimal places to show (Defaults to 4).
*
* @returns {string}  Amount in BCH units truncated to number of Decimal Places specified.
*/
export const satoshisToCashDisplay = function(satoshis: number, decimalPlaces: number = 4): string
{
	// Convert satoshis to Cash (BCH) Units.
	const inCashUnits = naiveSatoshisToCash(satoshis);

	// Truncate to the given number of decimal places.
	return inCashUnits.toFixed(decimalPlaces);
};

/**
* Check if the given address is a valid Cash Address (Base58 or CashAddr format).
*
* @function
*
* @param address {string} The address to check (Base58 or CashAddr format).
*
* @returns {Promise<boolean>} True if address is valid, false otherwise.
*/
export const isValidCashAddress = async function(address: string): Promise<boolean>
{
	// Add a prefix if necessary.
	let prefix = '';
	if(!address.startsWith('bitcoincash:'))
	{
		prefix = 'bitcoincash:';
	}

	// ...
	const sha256 = await instantiateSha256();

	// Determine what format the encoded address is in, if any.
	const addressIsBase58 = (typeof decodeBase58Address(sha256, address) !== 'string');
	const addressIsCashAddr = (typeof decodeCashAddress(prefix + address) !== 'string');

	// Evaluate whether the address is a valid address.
	const isValidAddress = addressIsCashAddr || addressIsBase58;

	return isValidAddress;
};

/**
* Check if the given address is a valid Smart Address.
*
* @function
*
* @param address {string} The address to check.
*
* @returns {Promise<boolean>} True if address is valid, false otherwise.
*/
export const isValidSmartAddress = async function(address: string): Promise<boolean>
{
	try
	{
		// Parse the bridge address.
		ethers.utils.getAddress(address);

		// Return true to indicate that the address is valid.
		return true;
	}
	catch(error)
	{
		// Return false to indicate that the address is invalid.
		return false;
	}
};

/**
* Check if the given Cash Transaction Hash looks valid.
*
* @function
*
* @param sourceTransaction {string} The transaction hash as hexadecimal (001122AABBCC...).
*
* @returns {Promise<boolean>} True if looks valid, false otherwise.
*/
export const looksLikeValidCashTransactionHash = async function(sourceTransaction: string): Promise<boolean>
{
	// Evaluate expressions against the Source Transaction string.
	const isCorrectLength = sourceTransaction.length === 64;
	const isHexadecimal = isHex(sourceTransaction);

	// If the source transaction is not a length of 64 characters, it is not valid.
	if(!isCorrectLength || !isHexadecimal)
	{
		return false;
	}

	return true;
};

/**
* Check if the given Smart Transaction Hash looks valid.
*
* @function
*
* @param sourceTransaction {string} The transaction hash (0x...).
*
* @returns {Promise<boolean>} True if looks valid, false otherwise.
*/
export const looksLikeValidSmartTransactionHash = async function(sourceTransaction: string): Promise<boolean>
{
	// Evaluate expressions against the Source Transaction string.
	const startsWith0x = sourceTransaction.startsWith('0x');
	const isCorrectLength = sourceTransaction.length === 66;
	const isHexadecimal = isHex(sourceTransaction.slice(2));

	// And then check expressions conditions.
	if(!startsWith0x || !isCorrectLength || !isHexadecimal)
	{
		return false;
	}

	return true;
};

/**
* Create a Cash Bridging Transaction.
*
* This is a transaction from the user's front end temporary Cash wallet to the backend receiving Cash wallet with an OP_RETURN to indicate the user's desired Smart Payout Address.
*
* Note that the user covers the fee for this transaction.
*
* @function
*
* @param privateKeyWIF    {string}                      Private Key in WIF format.
* @param unspentOutputs   {AddressListUnspentResponse}  List of Unspent Outputs to use.
* @param bridgeAddress    {string}                      Cash Receiving Address of Bridge.
* @param payoutAddress    {string}                      Smart Address bridge should pay out to.
* @param minerFeeSatoshis {number}                      The satoshis to pay as miner fee (deducted from value output).
*
* @returns {Uint8Array} The transaction binary.
*/
export const createCashBridgeTransaction = async function(privateKeyWIF: string, unspentOutputs: AddressListUnspentResponse, bridgeAddress: string, payoutAddress: string, minerFeeSatoshis: number = 0): Promise<Uint8Array>
{
	// Make sure the Bridge Address is a valid Cash Address.
	if(!await isValidCashAddress(bridgeAddress))
	{
		throw(new Error(`Invalid Cash Bridge Address given (${bridgeAddress}).`));
	}

	// Make sure the Payout Address is a valid Smart Address.
	if(!await isValidSmartAddress(payoutAddress))
	{
		throw(new Error(`Invalid Smart Payout Address given (${payoutAddress}).`));
	}

	// Calculate the total balance of the unspent outputs.
	const unspentSatoshis = unspentOutputs.reduce((totalValue, unspentOutput) => (totalValue + unspentOutput.value), 0);

	// Initialize an empty list of outputs.
	// NOTE: The order of the outputs we add to this is important and should be OP_RETURN, Value and Optional Change.
	const outputs = [];

	// Add the OP_RETURN data output.
	outputs.push(await createDataOutput(textEncoder.encode(payoutAddress)));

	// Add the value output (note that miner fee is deducted from output value).
	outputs.push(await createValueOutput(bridgeAddress, unspentSatoshis - minerFeeSatoshis));

	// Create the initial transaction to estimate miner fee.
	const transaction = await createTransaction(privateKeyWIF, unspentOutputs, outputs);

	// Return the transaction.
	return transaction;
};

/**
* Create a Cash Payout Transaction.
*
* This is a transaction from the Hop.Cash Outgoing/Liquidity wallet to the user's Cash Payout Address with an OP_RETURN to indicate the original Smart Source Transaction.
*
* Note that Hop.Cash covers the fee for this transaction.
*
* @function
*
* @param privateKeyWIF      {string}                      Private Key in WIF format.
* @param unspentOutputs     {AddressListUnspentResponse}  List of Unspent Outputs to use.
* @param payoutAddress      {string}                      The Bitcoin Cash Address to pay out to.
* @param satoshis           {number}                      The number of satoshis to pay out.
* @param sourceTransaction  {string}                      The source transaction for this request (without the 0x prefix).
* @param minerFeeSatoshis   {number}                      The satoshis to pay as miner fee (deducted from change).
*
* @returns {Uint8Array} The transaction binary.
*/
export const createCashPayoutTransaction = async function(privateKeyWIF: string, unspentOutputs: AddressListUnspentResponse, payoutAddress: string, satoshis: number, sourceTransaction: string, minerFeeSatoshis: number = 0): Promise<Uint8Array>
{
	// Make sure the Payout Address is a valid Cash Address.
	if(!await isValidCashAddress(payoutAddress))
	{
		throw(new Error(`Invalid Cash Bridge Address given (${payoutAddress}).`));
	}

	// Make sure that the Smart Source Transaction looks valid.
	// NOTE: We expect the 0x prefix to be omitted from the sourceTransaction.
	//       We cannot change this structure as this would break integration with partners.
	if(!await looksLikeValidSmartTransactionHash(`0x${sourceTransaction}`))
	{
		// TODO: Consider whether it is worth removing 0x for user instead of throwing?
		throw(new Error(`Smart Source Transaction (${sourceTransaction}) does not appear valid. Did you forget to omit the 0x prefix?`));
	}

	// Parse the Private Key WIF to get our change address.
	const [ , , changeAddress ] = await parseWIF(privateKeyWIF);

	// Calculate the total balance of the unspent outputs.
	const unspentSatoshis = unspentOutputs.reduce((totalValue, unspentOutput) => (totalValue + unspentOutput.value), 0);

	// Initialize an empty list of outputs.
	// NOTE: The order of the outputs we add to this is important and should be OP_RETURN, Value and optional Change.
	const outputs = [];

	// Add the OP_RETURN data output.
	outputs.push(await createDataOutput(hexToBin(sourceTransaction)));

	// Add the value output.
	outputs.push(await createValueOutput(payoutAddress, satoshis));

	// Initial change satoshis (miner fee not inclusive).
	const changeSatoshis = (unspentSatoshis - satoshis - minerFeeSatoshis);

	// Add the change output if it is more than the dust limit.
	if(changeSatoshis > CASH_SATOSHI_DUST_LIMIT)
	{
		outputs.push(await createValueOutput(changeAddress, (unspentSatoshis - satoshis - minerFeeSatoshis)));
	}

	// Create the transaction.
	const transaction = await createTransaction(privateKeyWIF, unspentOutputs, outputs);

	// Return the transaction.
	return transaction;
};

/**
* Create a Cash P2PKH Transaction.
*
* This is a generic P2PKH transaction for internal Hop.Cash use (e.g. Transferring Liquidity Balancer funds, setting up Wallets for Testing, etc).
*
* Note that fee is deducted from the value output.
*
* @function
*
* @param privateKeyWIF      {string}                      Private Key in WIF format.
* @param unspentOutputs     {AddressListUnspentResponse}  List of Unspent Outputs to use.
* @param targetAddress      {string}                      The Bitcoin Cash Address to pay out to.
* @param satoshis           {number}                      The number of satoshis to pay out.
* @param minerFeeSatoshis   {number}                      The satoshis to pay as miner fee (deducted from targetAddress).
*
* @returns {Uint8Array} The transaction binary.
*/
export const createCashP2PKHTransaction = async function(privateKeyWIF: string, unspentOutputs: AddressListUnspentResponse, targetAddress: string, satoshis: number, minerFeeSatoshis: number = 0): Promise<Uint8Array>
{
	// Make sure the Target Address is a valid Cash Address.
	if(!await isValidCashAddress(targetAddress))
	{
		throw(new Error(`Invalid Target Cash Address given (${targetAddress}).`));
	}

	// Parse the Private Key WIF to get our change address.
	const [ , , changeAddress ] = await parseWIF(privateKeyWIF);

	// Calculate the total balance of the unspent outputs.
	const unspentSatoshis = unspentOutputs.reduce((totalValue, unspentOutput) => (totalValue + unspentOutput.value), 0);

	// Initialize an empty list of outputs.
	const outputs = [];

	// Add the value output.
	// NOTE: We deduct the miner fee from our value output as opposed to change address.
	outputs.push(await createValueOutput(targetAddress, satoshis - minerFeeSatoshis));

	// Calculate the change amount.
	const changeSatoshis = (unspentSatoshis - satoshis);

	// Add the change output if it is more than the dust limit.
	if(changeSatoshis > CASH_SATOSHI_DUST_LIMIT)
	{
		outputs.push(await createValueOutput(changeAddress, (unspentSatoshis - satoshis)));
	}

	// Create the transaction.
	const transaction = await createTransaction(privateKeyWIF, unspentOutputs, outputs);

	// Return the transaction.
	return transaction;
};

/**
* Parse a Cash Bridge Transaction.
*
* This is a transaction from the user's front end temporary Cash wallet to the backend receiving Cash wallet with an OP_RETURN to indicate the user's desired Smart Payout Address.
*
* @function
*
* @param transactionHex  {string}  Transaction to parse as hex string.
*
* @throws {Error}  If Bridge Transaction cannot be parsed or is invalid.
*
* @returns {Promise<ParsedBridgeTransaction>}  The Parsed Cash Bridge Transaction.
*/
export const parseCashBridgeTransaction = async function(transactionHex: string): Promise<ParsedBridgeTransaction>
{
	// Get the transaction hash.
	const transactionHash = await getTransactionHash(hexToBin(transactionHex));

	// Decode the transaction and store the outputs.
	const { outputs } = decodeTransactionUnsafe(hexToBin(transactionHex));

	// Verify that transaction has the right number of outputs.
	if(outputs.length < 2)
	{
		throw(new Error(`Expected at least 2 outputs, transaction only contains ${outputs.length}`));
	}

	// The first output should be an OP_RETURN containing the payout address as data.
	const payoutDataOutput = outputs[0];

	// Get the payout address from this output.
	const payoutData = await getDataFromOutput(payoutDataOutput);

	// Decode the payout data into an address.
	const payoutAddress = textDecoder.decode(payoutData);

	// Make sure the SmartBCH Address is valid.
	if(!await isValidSmartAddress(payoutAddress))
	{
		throw(new Error(`Payout Address ${payoutAddress} in Bridge Transaction is not a valid Smart Address`));
	}

	// Get the bridge output (the output at the second position)..
	const bridgeOutput = outputs[1];

	// Get the bridge address.
	const bridgeAddress = await getAddressFromLockingBytecode(bridgeOutput.lockingBytecode);

	// Get the bridge amount in satoshis.
	const bridgeSatoshis = Number(binToBigIntUint64LE(bridgeOutput.satoshis));

	// Make sure the amount is not zero.
	if(!bridgeSatoshis)
	{
		throw(new Error('Cash Bridge Transaction did not pay any satoshis on second output (Bridge Receiving Address).'));
	}

	return { transactionHash, bridgeSatoshis, bridgeAddress, payoutAddress };
};

/**
* Parse a Smart Bridge Transaction.
*
* This is a transaction from the user's Smart wallet to the backend receiving Smart wallet with data attached to indicate the user's desired Cash Payout Address.
*
* @function
*
* @param transaction  {ethers.transaction}  Transaction to parse.
*
* @throws {Error}  If Bridge Transaction cannot be parsed or is invalid.
*
* @returns {Promise<ParsedBridgeTransaction>} The Parsed Bridge Transaction.
*/
export const parseSmartBridgeTransaction = async function(transaction: ethers.Transaction): Promise<ParsedBridgeTransaction>
{
	// Make sure the transaction hash is not a falsy value so that we can return it later.
	if(!transaction.hash)
	{
		throw(new Error('Smart Bridge Transaction invalid, did not contain a transaction hash.'));
	}

	// Make sure the to address is not a falsy value so that we can return it later.
	if(!transaction.to)
	{
		throw(new Error('Smart Bridge Transaction invalid, did not contain a "to" address.'));
	}

	// Get the transaction hash.
	const transactionHash = transaction.hash;

	// Get the bridge address.
	const bridgeAddress = transaction.to;

	// Get the payout address from the transaction data.
	// NOTE: We use substr(2) to remove the `0x...` part.
	const payoutAddress = textDecoder.decode(hexToBin(transaction.data.substr(2)));

	// Make sure the address is a valid Cash payout address.
	if(!await isValidCashAddress(payoutAddress))
	{
		throw(new Error(`Payout address (${payoutAddress}) is not a valid Cash Address.`));
	}

	// Convert the transaction value into the number of satoshis to bridge.
	// NOTE: transaction value returned is in Wei.
	const bridgeSatoshis = Math.floor(Number(ethers.utils.formatUnits(transaction.value, SATOSHI_TO_WEI_DECIMALS)));

	// Return our Parsed Smart Bridge Request.
	return { transactionHash, bridgeAddress, bridgeSatoshis, payoutAddress };
};

/**
* Parse a Cash Payout Transaction.
*
* This is a transaction from the Hop.Cash Outgoing/Liquidity wallet to the user's Cash Payout Address with an OP_RETURN to indicate the original Smart Source Transaction.
*
* @function
*
* @param transactionHex	{string}  Transaction Hex to parse.
*
* @throws {Error}  If Bridge Transaction cannot be parsed or is invalid.
*
* @returns {Promise<ParsedCashPayoutTransaction>}  The Parsed Cash Bridge Payout Transaction Hash.
*/
export const parseCashPayoutTransaction = async function(transactionHex: string): Promise<ParsedCashPayoutTransaction>
{
	// Get the transaction hash.
	const transactionHash = await getTransactionHash(hexToBin(transactionHex));

	// Decode the transaction and store the outputs.
	const { outputs } = decodeTransactionUnsafe(hexToBin(transactionHex));

	// Verify that transaction has the right number of outputs
	if(outputs.length < 2)
	{
		throw(new Error(`Expected at least 2 outputs, transaction only contains ${outputs.length}`));
	}

	// The first output should be an OP_RETURN containing the Source Transaction as data.
	const sourceTransactionDataOutput = outputs[0];

	// Get the Source Transaction from this output.
	const sourceTransactionData = await getDataFromOutput(sourceTransactionDataOutput);

	// Decode the Source Transaction into a hex string.
	// NOTE: The OP_RETURN data in a payout transaction does not include 0x, so we add this manually.
	const sourceTransaction = `0x${binToHex(sourceTransactionData)}`;

	// If the first output is a data output of the wrong length, ignore it.
	if(!await looksLikeValidSmartTransactionHash(sourceTransaction))
	{
		throw(new Error(`Smart Source Transaction appears to be invalid (${sourceTransaction}).`));
	}

	// Get the payout output (the second output).
	const payoutOutput = outputs[1];

	// Get the payout address.
	const payoutAddress = await getAddressFromLockingBytecode(payoutOutput.lockingBytecode);

	// Get the payout amount in satoshis.
	const payoutSatoshis = Number(binToBigIntUint64LE(payoutOutput.satoshis));

	// Return the request transaction hash.
	return { transactionHash, payoutSatoshis, payoutAddress, sourceTransaction };
};

/**
* Returns the number of confirmations required for a given Cash Bridge Transaction satoshi amount.
*
* @function
*
* @param satoshis	{number}  The satoshi amount of the bridge transaction.
*
* @returns {Promise<Number>}  The number of confirmations required.
*/
export const getRequiredCashConfirmations = async function(satoshis: number): Promise<number>
{
	if(satoshis > CASH_REQUIRES_TWO_CONFIRMATIONS_THRESHOLD * SATOSHIS_PER_BITCOIN_CASH) return 2;
	if(satoshis > CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD * SATOSHIS_PER_BITCOIN_CASH) return 1;

	return 0;
};

/**
* Calculates the Hop.Cash Bridge Fee for a given Satoshi amount.
*
* @function
*
* @param satoshis	{string}  The satoshi amount of the bridge transaction.
*
* @returns {Promise<Number>}  The bridge fee in satoshi units.
*/
export const calculateBridgeFee = async function(satoshis: number): Promise<number>
{
	return Math.ceil(Math.max(satoshis * FEE_RATE_MULTIPLIER, MINIMUM_FEE_SATOSHIS));
};

/**
 * Derives a Version 1 Hop Wallet (frontend) Private Key given a signature.
 *
 * @function
 *
 * @param signature Signature generated using using the Ethers signMessage() function.
 *
 * @returns {Promise<Uint8Array>} A 32 byte Private Key as Uint8Array.
 */
export const deriveLegacyHopWalletPrivateKey = async function(signature: string): Promise<Uint8Array>
{
	// Generate signature hash and entropy.
	const signatureHash = ethers.utils.sha256(signature);

	// Convert the signature to an Ethers Big Number.
	// NOTE: We do this as Hop.Cash V1 derivation pads the missing bytes with null bytes *at the end*.
	//       Where as our Ethers signature hash will have the null bytes *at the beginning*.
	const signatureAsBigNumber = ethers.BigNumber.from(signatureHash);

	// Format the Private Key to binary.
	// NOTE: Start at position 2 to omit the 0x prefix added by toHexString.
	let privateKey = hexToBin(signatureAsBigNumber.toHexString().substring(2));

	// If our resulting signature is not 32 bytes due to the conversion to a number (i.e. it's been truncated)...
	if(privateKey.length !== 32)
	{
		// Create a 32 byte array to hold the corrected Private Key.
		const correctedPrivateKey = new Uint8Array(32);

		// Fill all of our 32 bytes with null (0x00).
		correctedPrivateKey.fill(0);

		// Copy the incorrect Private Key bytes into the corrected Private Key.
		// NOTE: This will only copy a many bytes as are in our incorrect Private Key.
		//       This means that any missing bytes at the end will be padded with null (0x00) as per Hop Cash Version 1 derivation.
		correctedPrivateKey.set(privateKey);

		// Set the Private Key that we will return to this corrected Private Key.
		privateKey = correctedPrivateKey;
	}

	// Return the Private Key.
	return privateKey;
};

/**
 * Derives a Version 2 Hop Wallet (frontend) Private Key given a signature.
 *
 * @function
 *
 * @param signature Signature generated using using the Ethers signMessage() function.
 *
 * @returns {Promise<Uint8Array>} A 32 byte Private Key as Uint8Array.
 */
export const deriveHopWalletPrivateKey = async function(signature: string): Promise<Uint8Array>
{
	// Instantiate Libauth SHA256 instance.
	const sha256 = await instantiateSha256();

	// Generate a SHA256 hash of our signature.
	// NOTE: Start at position 2 to omit the 0x prefix added by Ethers sign message function.
	const signatureHash = sha256.hash(hexToBin(signature.substring(2)));

	// Convert our signature to a BigInt so that we can perform a modulo operation using the Hop Wallet Prime Number.
	const signatureEntropy = binToBigIntUint256BE(signatureHash);

	// Convert the Hop Cash Prime into a BigInt.
	const hopWalletPrimeAsBigInt = BigInt(HOP_WALLET_PRIME);

	// Perform the modulo operation on our signature entropy to derive our Private Key.
	const privateKey = bigIntToBinUint256BEClamped(hopWalletPrimeAsBigInt % signatureEntropy);

	// Return the Private Key.
	return privateKey;
};
