/**
 * @module Constants
 * @description Constants used by the Hop.Cash Frontend and Backend.
 */

/** The relation between satoshis and wei. */
export const SATOSHI_TO_WEI_DECIMALS = 10;

/** Number of satoshis in a bitcoin cash. */
export const SATOSHIS_PER_BITCOIN_CASH = 100_000_000;

/** Number of milliseconds in a second. */
export const MILLISECONDS_PER_SECOND = 1000;

/** Application Fee Rate Multiplier. */
export const FEE_RATE_MULTIPLIER  = 0.001;

/** The minimum number of Satoshis that can be sent to the Bridge for a Bridge Request to be considered valid.
 * NOTE: We enforce 0.01 on the frontend, but set a far more lenient threshold on backend to accommodate transaction fees.
 */
export const MINIMUM_BACKEND_SATOSHIS = Math.round(0.005 * SATOSHIS_PER_BITCOIN_CASH);

/** The minimum number of satoshis that are accepted on the frontend for a Bridge Request. */
export const MINIMUM_FRONTEND_SATOSHIS = Math.round(0.01 * SATOSHIS_PER_BITCOIN_CASH);

/** The minimum fee that will ever be paid on any Bridge Request. */
export const MINIMUM_FEE_SATOSHIS = Math.round(0.0001 * SATOSHIS_PER_BITCOIN_CASH);

/** Cash to Smart Direction (as number). */
export const CASH_TO_SMART = 1;

/** Smart to Cash Direction (as number). */
export const SMART_TO_CASH = 2;

/** Prime Number used when deriving Hop Wallet. */
export const HOP_WALLET_PRIME = '0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364140';

/**
 * Fork ID - Sighash Type is technically four bytes. BCH only uses one byte currently.
 *
 * See: https://documentation.cash/protocol/forks/replay-protected-sighash
 */
export const CASH_FORK_ID = new Uint8Array([ 0, 0, 0 ]);

/** Satoshi Fee per Byte of data */
export const CASH_MINER_FEE_IN_SATOSHIS_PER_BYTE = 1;

/** Threshold amount in Cash (BCH) Units for requiring one confirmation. */
export const CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD = 5;

/** Threshold amount in Cash (BCH) Units for requiring two confirmations. */
export const CASH_REQUIRES_TWO_CONFIRMATIONS_THRESHOLD = 50;

/** Minimum of satoshis accepted in a value output by the Bitcoin Cash Network. */
export const CASH_SATOSHI_DUST_LIMIT = 546;

/** SmartBCH Chain ID (as a Number). */
export const SMART_CHAIN_ID = 10000;

/** SmartBCH Chain ID (as a Hexadecimal). */
export const SMART_CHAIN_ID_HEX = '0x2710';

/** SmartBCH Chain Name. */
export const SMART_CHAIN_NAME = 'Smart Bitcoin Cash';

/** SmartBCH Unit's Name. */
export const SMART_CHAIN_UNIT_NAME = 'SmartBCH';

/** SmartBCH Unit's Symbol. */
export const SMART_CHAIN_UNIT_SYMBOL = 'BCH';

/** SmartBCH Unit Decimals (Gwei). */
export const SMART_CHAIN_UNIT_DECIMALS = 18;

/** Array of SmartBCH explorer URLs. */
export const SMART_CHAIN_EXPLORER_URLS =
[
	'https://www.smartscan.cash',
];

/** Array of SmartBCH RPC URLs. */
export const SMART_CHAIN_RPC_URLS =
[
	'https://smartbch.greyh.at',
	'https://smartbch.fountainhead.cash/mainnet',
	'https://global.uat.cash/',
];

/** SmartBCH Chain Parameters. */
export const SMART_CHAIN_PARAMETERS =
{
	chainId: SMART_CHAIN_ID_HEX,
	chainName: SMART_CHAIN_NAME,
	nativeCurrency:
	{
		name: SMART_CHAIN_UNIT_NAME,
		symbol: SMART_CHAIN_UNIT_SYMBOL,
		decimals: SMART_CHAIN_UNIT_DECIMALS,
	},
	blockExplorerUrls: SMART_CHAIN_EXPLORER_URLS,
	rpcUrls: SMART_CHAIN_RPC_URLS,
};

/** SmartBCH Deployed Contract Address */
export const SMART_CONTRACT_ADDRESS = '0xBAe8Af26E08D3332C7163462538B82F0CBe45f2a';

/** SmartBCH Contract's Application Binary Interface */
export const SMART_CONTRACT_ABI = [
	'function bridge(bytes32[] calldata sourceTransactions, address[] calldata outputAddresses, uint256[] calldata outputAmounts) external payable',
	'event Bridged(bytes32 indexed sourceTransaction, address indexed liquidityProviderAddress, address indexed outputAddress, uint256 outputAmount)',
];

/** Fixed gas rate for sending to a SmartBCH Externally Owned Address.
 * For background on this 21,000 constant, see:
 * https://eth.wiki/en/fundamentals/design-rationale
 * https://ethereum-magicians.org/t/some-medium-term-dust-cleanup-ideas/6287
 */
export const SMART_EXTERNALLY_OWNED_ADDRESS_TRANSACTION_GAS = 21_000;
