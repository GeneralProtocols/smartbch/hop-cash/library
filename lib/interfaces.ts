/**
* @module Interfaces
*/

/** Parsed Bridge Request */
export interface ParsedBridgeTransaction
{
	/** The hash of this transaction as a hexadecimal string. */
	transactionHash: string;

	/** The address of the bridge this request was sent to. */
	bridgeAddress: string;

	/** The amount in satoshi units sent to bridge. */
	bridgeSatoshis: number;

	/** The Address which bridge should pay out to. */
	payoutAddress: string;
}

/** Parsed Cash Payout Transaction */
export interface ParsedCashPayoutTransaction
{
	/** The hash of this transaction as a hexadecimal string. */
	transactionHash: string;

	/** The Cash Payout address */
	payoutAddress: string;

	/** The number of satoshis paid out. */
	payoutSatoshis: number;

	/** The Smart Source Transaction */
	sourceTransaction: string;
}
