/**
* @module Electrum
* @description Wrapper functions for making calls to Electrum Clusters.
*/

// Import types for related electrum calls.
import type { HeadersSubscribeResponse, AddressGetBalanceResponse, AddressGetHistoryResponse, AddressListUnspentResponse, TransactionGetHeightResponse, TransactionDsProofGetResponse, TransactionGetVerboseResponse, TransactionBroadcastResponse } from './electrum-types';

// Import Electrum Or Data Error class.
import { ElectrumOrDataError } from './electrum-types';

// Import library to communicate with the Bitcoin Cash chain/network.
import { ElectrumCluster, ElectrumClient, RequestResponse } from 'electrum-cash';

// Import LibAuth utilities.
import { binToHex, hexToBin, instantiateSha256, isHex, getTransactionHashLE } from '@bitauth/libauth';

// TODO: The functionality in this file pertains to the Bitcoin Cash Network.
//       In future, once a GP Wallet Library is available, we should ideally leverage that.

/**
 * Create an Electrum Cluster.
 *
 * @function
 *
 * @param serverUrls {Array<string>} Array of Electrum Provider URLs that should be leveraged by the cluster.
 *
 * @return {Promise<ElectrumCluster>} The created Electrum Cluster.
 */
export const createElectrumCluster = async function(serverUrls: Array<string>): Promise<ElectrumCluster>
{
	// Create a new electrum cluster, with the default 1-of-all configuration.
	const electrum = new ElectrumCluster('hop.cash', '1.4.5');

	// Add some servers to the cluster.
	for(const server of serverUrls)
	{
		await electrum.addServer(server);
	}

	// Wait for enough connections to be available.
	await electrum.ready();

	// Return the connected cluster.
	return electrum;
};

/**
 * Get the current block height of the network.
 *
 * @function
 *
 * @param electrum {ElectrumCluster|ElectrumClient} The Electrum Cluster/Client to use.
 *
 * @throws {ElectrumOrDataError}  If Internal Electrum Error occurred or response did not match expected response.
 *
 * @return {Promise<number>} The current block height.
 */
export const getCurrentHeight = async function(electrum: ElectrumCluster | ElectrumClient): Promise<number>
{
	try
	{
		// Fetch the current block height by requesting a subscription on the block headers.
		// NOTE: The subscription have a negligible backend-cost and includes the block height in the response.
		const response = await electrum.request('blockchain.headers.subscribe') as (HeadersSubscribeResponse | Error);

		// Make sure our Electrum Call did not return an Error.
		if(response instanceof Error)
		{
			throw(new Error(`Call to blockchain.headers.subscribe failed: ${response}`));
		}

		// Evaluate whether the response is a regular object.
		const isRegularObject = (typeof response === 'object' && response !== null);

		// Evaluate whether the response contains "height" as a number.
		const hasExpectedProperties = (typeof response?.height === 'number');

		// Throw an error if the returned data is not a regular object or does not contain our expected properties.
		if(!isRegularObject || !hasExpectedProperties)
		{
			throw(new Error(`Expected response to be a regular object containing height. Got "${typeof response}" (${response}).`));
		}

		// Extract the height from the response.
		const { height } = response;

		// Return the height as a number.
		return Number(height);
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to get current block height: ${error}`));
	}
};

/**
 * Get the confirmed block height for a transaction.
 *
 * If transaction is confirmed, positive number is returned.
 * If transaction is in mempool, 0 (zero) is returned.
 * If transaction is neither confirmed nor in mempool, Error is thrown.
 *
 * @function
 *
 * @param electrum        {ElectrumCluster|ElectrumClient} The Electrum Cluster/Client to use.
 * @param transactionHash {string}                         The transaction hash as a hex string.
 *
 * @throws {ElectrumOrDataError}  If transaction is unknown or response did not match expected response.
 *
 * @return {Promise<number>} The confirmed block height of the transaction.
 */
export const getTransactionHeight = async function(electrum: ElectrumCluster | ElectrumClient, transactionHash: string): Promise<number>
{
	try
	{
		// Fetch the transactions confirmed height
		const response = await electrum.request('blockchain.transaction.get_height', transactionHash) as (TransactionGetHeightResponse | Error);

		// Make sure our Electrum Call did not return an Error.
		// Known Errors: "No transaction matching the requested hash was found"
		if(response instanceof Error)
		{
			throw(new Error(`Call to blockchain.transaction.get_height failed: ${response}`));
		}

		// Throw an error if the returned data is not a number as we expect it to be.
		if(typeof response !== 'number')
		{
			throw(new Error(`Expected response to be a number. Got "${typeof response}" (${response}).`));
		}

		// Return the height as a number.
		return Number(response);
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to get confirmed height for transaction (${transactionHash}): ${error}`));
	}
};

/**
 * Get the unspent outputs on a given P2PKH address.
 *
 * @function
 *
 * @param electrum {ElectrumCluster|ElectrumClient} The Electrum Cluster/Client to use.
 * @param address  {string}                         The P2PKH address.
 *
 * @throws {ElectrumOrDataError}  If Internal Electrum Error occurred or response did not match expected response.
 *
 * @return {Promise<AddressListUnspentResponse>} The list of unspent outputs.
 */
export const getUnspentOutputs = async function(electrum: ElectrumCluster | ElectrumClient, address: string): Promise<AddressListUnspentResponse>
{
	try
	{
		// Fetch a list of unspent outputs for the provided address.
		const response = await electrum.request('blockchain.address.listunspent', address) as (AddressListUnspentResponse | Error);

		// Make sure our Electrum Call did not return an Error.
		// Known Errors: "Invalid address: ${address}"
		if(response instanceof Error)
		{
			throw(new Error(`Call to blockchain.address.listunspent failed: ${response}`));
		}

		// Throw an error if the returned data is not an array as we expect it to be.
		if(!Array.isArray(response))
		{
			throw(new Error(`Expected response to be an array. Got "${typeof response}" (${response}).`));
		}

		// Return the response that contains the unspent outputs.
		return response;
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to get unspent outputs: ${error}`));
	}
};

/**
 * Get a transaction as a raw hexadecimal string.
 *
 * @function
 *
 * @param electrum        {ElectrumCluster|ElectrumClient} The Electrum Cluster/Client to use.
 * @param transactionHash {string}                         The transaction hash as a hex string.
 *
 * @throws {ElectrumOrDataError}  If Internal Electrum Error occurred or response did not match expected response.
 *
 * @return {Promise<string>} The transaction as a hexadecimal string.
 */
export const getTransaction = async function(electrum: ElectrumCluster | ElectrumClient, transactionHash: string): Promise<string>
{
	try
	{
		// Fetch transaction as a hexadecimal string.
		const response = await electrum.request('blockchain.transaction.get', transactionHash) as (string | Error);

		// Make sure our Electrum Call did not return an Error.
		// Known Errors: "No such mempool or blockchain transaction. Use gettransaction for wallet transactions."
		if(response instanceof Error)
		{
			throw(new Error(`Call to blockchain.transaction.get failed: ${response}`));
		}

		// Evaluate whether the response is a hexadecimal string representing a transaction.
		const isHexadecimal = isHex(response);

		// Throw an error if the returned data is not a hexadecimal string as we expect it to be.
		if(!isHexadecimal)
		{
			throw(new Error(`Expected response to be a hexadecimal representation of transaction. Got "${typeof response}" (${response}).`));
		}

		// Return the information returned.
		return response;
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to get transaction: ${error}`));
	}
};

/**
 * Get a transaction in Electrum's verbose (JSON) format.
 *
 * @function
 *
 * @param electrum        {ElectrumCluster|ElectrumClient} The Electrum Cluster/Client to use.
 * @param transactionHash {string}                         The transaction hash as a hex string.
 *
 * @throws {ElectrumOrDataError}  If Internal Electrum Error occurred or response did not match expected response.
 *
 * @return {Promise<TransactionGetVerboseResponse>} The transaction in Electrum's verbose (JSON) format.
 */
export const getTransactionVerbose = async function(electrum: ElectrumCluster | ElectrumClient, transactionHash: string): Promise<TransactionGetVerboseResponse>
{
	try
	{
		// Fetch verbose information about a transaction.
		// NOTE: This can vary somewhat based on what node is replying to the request.
		const response = await electrum.request('blockchain.transaction.get', transactionHash, true) as (TransactionGetVerboseResponse | Error);

		// Make sure our Electrum Call did not return an Error.
		// Known Errors: "No such mempool or blockchain transaction. Use gettransaction for wallet transactions."
		if(response instanceof Error)
		{
			throw(new Error(`Call to blockchain.transaction.get failed: ${response}`));
		}

		// Evaluate whether the response is a regular object.
		const isRegularObject = (typeof response === 'object' && response !== null);

		// Throw an error if the returned data is not a regular object as we expect it to be.
		if(!isRegularObject)
		{
			throw(new Error(`Expected response to be a regular object. Got "${typeof response}" (${response}).`));
		}

		// Return the information returned.
		return response;
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to get verbose transaction information: ${error}`));
	}
};

/**
 * Get the Double-Spend-Proof (DSP) for a given transaction.
 *
 * @function
 *
 * @param electrum        {ElectrumCluster} The Electrum Cluster to use.
 * @param transactionHash {string}          The transaction hash as a hex string.
 *
 * @throws {ElectrumOrDataError}  If Internal Electrum Error occurred or response did not match expected response (Object or null).
 *
 * @return {Promise<TransactionDsProofGetResponse>} The Double-Spend-Proof of a transaction.
 */
export const getDoubleSpendProof = async function(electrum: ElectrumCluster | ElectrumClient, transactionHash: string): Promise<TransactionDsProofGetResponse>
{
	try
	{
		// Fetch existing double-spend proofs for a transaction.
		const response = await electrum.request('blockchain.transaction.dsproof.get', transactionHash) as (TransactionDsProofGetResponse | Error);

		// Make sure our Electrum Call did not return an Error.
		if(response instanceof Error)
		{
			throw(new Error(`Call to blockchain.transaction.dsproof.get failed: ${response}`));
		}

		// Evaluate whether the response is an object or null value.
		const isObject = (typeof response === 'object');
		const isNull = (response === null);

		// Throw an error if the response is not an object and is it not null as we expect it to be.
		if(!isObject && !isNull)
		{
			throw(new Error(`Expected response to be a regular object or null value. Got "${typeof response}" (${response}).`));
		}

		// Return any double spend proofs, or lack thereof.
		return response;
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to get double spend proof for transaction (${transactionHash}): ${error}`));
	}
};

/**
 * Get the Double-Spend-Proof (DSP) score for a given transaction.
 *
 * @function
 *
 * @param electrum        {ElectrumCluster|ElectrumClient} The Electrum Cluster/Client to use.
 * @param transactionHash {string}                         The transaction hash as a hex string.
 *
 * @throws {ElectrumOrDataError}  If Internal Electrum Error occurred or response did not match expected response.
 *
 * @return {Promise<number>} The Double-Spend-Proof score.
 */
export const getDoubleSpendProofScore = async function(electrum: ElectrumCluster | ElectrumClient, transactionHash: string): Promise<number>
{
	try
	{
		// Fetch existing double-spend proofs for a transaction.
		const response = await electrum.request('bchn.getdsproofscore', transactionHash) as any;

		// Make sure our Electrum Call did not return an Error.
		// Known Errors: "Transaction not in mempool"
		if(response instanceof Error)
		{
			throw(new Error(`Call to bchn.getdsproofscore failed: ${response}`));
		}

		// Throw an error if the returned data is not a number as we expect it to be.
		if(typeof response !== 'number')
		{
			throw(new Error(`Expected response to be a number. Got "${typeof response}" (${response}).`));
		}

		// Return any double spend proofs, or lack thereof.
		return response;
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to get double spend proof for transaction (${transactionHash}): ${error}`));
	}
};

/**
 * Broadcast a transaction to the network.
 *
 * @function
 *
 * @param electrum       {ElectrumCluster|ElectrumClient} The Electrum Cluster/Client to use.
 * @param transactionHex {string}                         The transaction to broadcast as a hex string.
 * @param waitForIndexer {boolean}                        TODO: Document me.
 *
 * @throws {ElectrumOrDataError}  If Internal Electrum Error occurred or response did not match expected response.
 *
 * @return {Promise<TransactionBroadcastResponse>} The result of the transaction.
 */
export const broadcastTransaction = async function(electrum: ElectrumCluster | ElectrumClient, transactionHex: string, waitForIndexer: boolean = false): Promise<TransactionBroadcastResponse>
{
	try
	{
		let updateIndexerPromise;

		// If we should wait for the indexer to update..
		if(waitForIndexer)
		{
			const promiseEvaluator = async function(resolve: Function): Promise<void>
			{
				// Create a function that resolves the promise if the transaction is recognized by the indexer.
				// NOTE: Only fulcrum has this transaction subscription. It returns a result that is identical to get_height on the transaction.
				const resolvePromiseWhenIndexed = function(indexedHeight: Error | RequestResponse): void
				{
					// NOTE: the indexed height is null for unknown, 0 when in mempool and 1+ when confirmed.
					if(indexedHeight !== null)
					{
						resolve(true);
					}
				};

				// Before broadcasting, we subscribe to the state of the transaction.
				await electrum.subscribe(resolvePromiseWhenIndexed, 'blockchain.transaction.subscribe', transactionHex);
			};

			// Create a promise that that we resolve when the indexer has updated.
			updateIndexerPromise = new Promise(promiseEvaluator);
		}
		else
		{
			// Set the update indexer promise to be resolved prior to broadcasting the transaction.
			updateIndexerPromise = Promise.resolve(true);
		}

		// Broadcast the provided transaction to the network.
		const response = await electrum.request('blockchain.transaction.broadcast', transactionHex) as (TransactionBroadcastResponse | Error);

		// Make sure our Electrum Call did not return an Error.
		// Known Errors: "the transaction was rejected by network rules."
		if(response instanceof Error)
		{
			throw(new Error(`Call to blockchain.transaction.broadcast failed: ${response}`));
		}

		// Instantiate LibAuth SHA256 instance.
		const sha256 = await instantiateSha256();

		// Calculate the transaction hash of the broadcasted transaction.
		const transactionHash = binToHex(getTransactionHashLE(sha256, hexToBin(transactionHex)));

		// If the broadcast was successful, wait for the indexer promise.
		// NOTE: This will already be resolved if we did not choose to wait for the indexer.
		if(response === transactionHash)
		{
			await updateIndexerPromise;
		}

		// Return the transaction hash of the broadcasted transaction.
		return response;
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to broadcast transaction: ${error}`));
	}
};

/**
 * Get the balance on a given P2PKH address.
 *
 * @function
 *
 * @param electrum {ElectrumCluster|ElectrumClient} The Electrum Cluster/Client to use.
 * @param address  {string}                         The P2PKH address.
 *
 * @throws {ElectrumOrDataError}  If Internal Electrum Error occurred or response did not match expected response.
 *
 * @return {Promise<number>} The summed balance of confirmed and unconfirmed transactions.
 */
export const getBalance = async function(electrum: ElectrumCluster | ElectrumClient, address: string): Promise<number>
{
	try
	{
		// Request the current balance on the address.
		const response = await electrum.request('blockchain.address.get_balance', address) as (AddressGetBalanceResponse | Error);

		// Make sure our Electrum Call did not return an Error.
		// Known errors: "Invalid address: ${address}"
		if(response instanceof Error)
		{
			throw(new Error(`Call to blockchain.address.get_balance failed: ${response}`));
		}

		// Evaluate whether the response is a regular object.
		const isRegularObject = (typeof response === 'object' && response !== null);

		// Evaluate whether the response contains "confirmed" and "unconfirmed" as numbers as we expect it to.
		const hasExpectedProperties = (typeof response?.confirmed === 'number' && typeof response?.unconfirmed === 'number');

		// Throw an error if response is not a regular object or does not have our expected properties.
		if(!isRegularObject || !hasExpectedProperties)
		{
			throw(new Error(`Expected response to be a regular object containing 'confirmed' and 'unconfirmed' as numbers. Got "${typeof response}" (${response}).`));
		}

		// Return the sum of the confirmed and unconfirmed balance.
		return response.confirmed + response.unconfirmed;
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to get balance on address (${address}): ${error}`));
	}
};

/**
 * Return the confirmed and unconfirmed history of a Bitcoin Cash address.
 *
 * Note that Legacy Address is not formally supported by specification (but may work with some Electrum Servers).
 *
 * @function
 *
 * @param electrum {ElectrumCluster|ElectrumClient} The Electrum Cluster/Client to use.
 * @param address  {string}                         Address in Cash Addr format.
 *
 * @throws {ElectrumOrDataError}  If Internal Electrum Error occurred or response did not match expected response.
 *
 * @return {Promise<AddressGetHistoryResponse>} Address History as Array of objects with tx_hash and height.
 */
export const getHistory = async function(electrum: ElectrumCluster | ElectrumClient, address: string): Promise<AddressGetHistoryResponse>
{
	try
	{
		// Fetch history for the given address.
		const response = await electrum.request('blockchain.address.get_history', address) as (AddressGetHistoryResponse | Error);

		// Make sure our Electrum Call did not return an Error.
		// Known Errors: "Invalid address ${address}":
		if(response instanceof Error)
		{
			throw(new Error(`Call to blockchain.address.get_history failed: ${response}`));
		}

		// Throw an error if the returned data is not an array as we expect it to be.
		if(!Array.isArray(response))
		{
			throw(new Error(`Expected response to be an array. Got "${typeof response}" (${response}).`));
		}

		// Return address history.
		return response;
	}
	catch(error)
	{
		throw(new ElectrumOrDataError(`Failed to get history for address (${address}): ${error}`));
	}
};
