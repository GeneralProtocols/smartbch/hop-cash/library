/**
 * @module Utils
 * @description Various Bitcoin Cash utility functions for use with Hop.Cash.
 */

// Import necessary constants.
import { CASH_FORK_ID } from './constants';

// Import necessary Electrum Types.
import type { AddressListUnspentEntry, AddressListUnspentResponse } from './electrum-types';

// Import LibAuth functionality necessary.
import { AddressType, CashAddressNetworkPrefix, instantiateSha256, decodePrivateKeyWif, hexToBin, bigIntToBinUint64LE, flattenBinArray, encodeDataPush, encodeTransaction, getTransactionHashLE, instantiateSecp256k1, lockingBytecodeToAddressContents, createTransactionContextCommon, generateSigningSerializationBCH, binToHex, cashAddressToLockingBytecode, base58AddressToLockingBytecode, instantiateRipemd160, encodeCashAddress, parseBytecode, CashAddressType, Input, Transaction, Output } from '@bitauth/libauth';

// TODO: A lot of the functionality in this file pertains to Transaction Creation/Decoding.
//       In future, once a GP Wallet Library is available, we should ideally leverage that.

/**
* Parse a WIF string into a private key, public key and address.
*
* @function
*
* @param wif {string} Wallet in WIF format.
*
* @returns {Promise<Array<String>>} Array containing [0] Private Key, [1] Public Key and [2] Address.
*/
export const parseWIF = async function(wif: string): Promise<Array<string>>
{
	// Instantiate Libauth crypto interfaces
	const secp256k1 = await instantiateSecp256k1();
	const sha256 = await instantiateSha256();
	const ripemd160 = await instantiateRipemd160();

	// Attempt to decode WIF string into a private key
	const decodeResult = decodePrivateKeyWif(await instantiateSha256(), wif);

	// If decodeResult is a string, it represents an error, so we throw it.
	if(typeof decodeResult === 'string') throw(new Error(decodeResult));

	// Extract the private key from the decodeResult.
	const privateKeyBin = decodeResult.privateKey;

	// Derive the corresponding public key.
	const publicKeyBin = secp256k1.derivePublicKeyCompressed(privateKeyBin);

	// Hash the public key hash according to the P2PKH scheme.
	const publicKeyHashBin = ripemd160.hash(sha256.hash(publicKeyBin));

	// Encode the public key hash into a P2PKH cash address.
	const address = encodeCashAddress('bitcoincash', CashAddressType.P2PKH, publicKeyHashBin);

	return [ binToHex(privateKeyBin), binToHex(publicKeyBin), address ];
};

/**
* Get OP_RETURN Data from locking code.
*
* @function
*
* @param output {Output} Output that should be an OP_RETURN (data) output.
*
* @throws {Error} If output is not an OP_RETURN output.
*
* @returns {Promise<Uint8Array>} The OP_RETURN data.
*/
export const getDataFromOutput = async function(output: Output): Promise<Uint8Array>
{
	// Define OP_RETURN constant for legibility. (0x6A)
	const OP_RETURN = 106;

	// If the lockingBytecode is not a data output, throw an error.
	if(output.lockingBytecode[0] !== OP_RETURN)
	{
		throw(new Error('Locking Bytecode is not a data (OP_RETURN) output.'));
	}

	// Parse the bytecode of this OP_RETURN output.
	// NOTE: The below should support OP_RETURN scripts of the form:.
	//       OP_RETURN {length] {data}
	//       OP_RETURN OP_PUSHDATA{X} {length} {data}
	const payoutDataInstructions = parseBytecode(output.lockingBytecode);

	// Ensure locking bytecode contains two instructions.
	if(payoutDataInstructions.length !== 2)
	{
		throw(new Error('Locking bytecode does not contain two instructions.'));
	}

	// Get the instruction at index 1 (we expect and check below that this is a PUSH operation).
	const pushInstruction = payoutDataInstructions[1];

	// Ensure the instruction is an OP PUSH operation (any OP_CODE above 78/0x4e IS NOT a PUSH operation).
	if(pushInstruction.opcode > 78)
	{
		throw(new Error('Instruction at index 1 is not a push operation.'));
	}

	// Make sure the PUSH operation contains the expected data property.
	if(!('data' in pushInstruction))
	{
		throw(new Error('Instruction at index 1 does not contain a data property'));
	}

	// Return the data.
	return pushInstruction.data;
};

/**
* Get Cash Address from Locking Bytecode.
*
* @function
*
* @param lockingBytecode {Uint8Array} Locking Bytecode.
*
* @throws {Error} If locking bytecode is not a valid P2PKH address.
*
* @returns {string} The Bitcoin Cash Address.
*/
export const getAddressFromLockingBytecode = async function(lockingBytecode: Uint8Array): Promise<string>
{
	// Detect the address type based on the locking code.
	const addressContents = lockingBytecodeToAddressContents(lockingBytecode);

	// Make sure it is a P2PKH Address.
	if(addressContents.type !== AddressType.p2pkh)
	{
		throw(new Error(`Locking Bytecode is not a P2PKH address. Detected ${addressContents.type}.`));
	}

	// Get the address.
	const address = encodeCashAddress(CashAddressNetworkPrefix.mainnet, CashAddressType.P2PKH, addressContents.payload);

	return address;
};

/**
* Converts an address to its locking byte-code equivalent.
*
* @function
*
* @param address {string} Bitcoin Cash address.
*
* @returns {Uint8Array} The locking code for the given address.
*/
export const getLockingBytecodeFromAddress = async function(address: string): Promise<Uint8Array>
{
	// Initialize an empty error message, that we can use to display after we exhausted our options.
	let errorMessages = '';

	try
	{
		// Add a prefix if necessary.
		let prefix = '';
		if(!address.startsWith('bitcoincash:'))
		{
			prefix = 'bitcoincash:';
		}

		const lockScriptResult = cashAddressToLockingBytecode(prefix + address);

		// Throw an error in case of failure (which we'll catch and ignore).
		if(typeof lockScriptResult === 'string')
		{
			throw(new Error(`Cannot decode '${address}' as a cash address: ${lockScriptResult}`));
		}

		return lockScriptResult.bytecode;
	}
	catch(error)
	{
		// Store the error message, but otherwise do nothing.
		errorMessages += error;
	}

	try
	{
		// Attempt to decode the address as a base58 legacy address.
		const sha256 = await instantiateSha256();
		const lockScriptResult = base58AddressToLockingBytecode(sha256, address);

		// Throw an error in case of failure (which we'll catch and ignore).
		if(typeof lockScriptResult === 'string')
		{
			throw(new Error(`Cannot decode '${address}' as a base58 address: ${lockScriptResult}`));
		}

		return lockScriptResult.bytecode;
	}
	catch(error)
	{
		// Store the error message, but otherwise do nothing.
		errorMessages += error;
	}

	// Throw an error, including the the most recent error message in case the address could not be decoded with either address type.
	throw(new Error(`Failed to decode '${address}': ${errorMessages}`));
};

/**
* Utility function to convert an electrum unspent output to a libauth compatible input.
*
* @function
*
* @param unspentOutput {AddressListUnspentEntry} Unspent Output to create input from.
*
* @returns {any} The created input.
*/
export const createUnsignedInput = function(unspentOutput: AddressListUnspentEntry): any
{
	const input =
	{
		outpointIndex: unspentOutput.tx_pos,
		outpointTransactionHash: hexToBin(unspentOutput.tx_hash),
		unlockingBytecode: new Uint8Array(),
		satoshis: unspentOutput.value,
		sequenceNumber: 0,
	};

	// TODO: We should try to strongly type the return type of this function.
	// This is essentially a LibAuth "Input" interface, but has the field "satoshis" added: https://libauth.org/interfaces/input.html
	// Refactoring this would require refactoring how we call unlockP2PKHInput().
	return input;
};

/**
* Create the signing serialization for a given transaction input.
*
* @function
*
* @param transaction        {Transaction} The transaction to use.
* @param satoshis           {number}      The input's satoshi value.
* @param inputIndex         {number}      Input index to sign.
* @param coveredBytecodeBin {Uint8Array}  The input's locking script.
* @param hashtype           {number}      Hash type to use for signing serialization.
*
* @returns {Promise<Uint8Array>}	The signing serialization.
*/
export const createSigningSerialization = async function(transaction: Transaction, satoshis: number, inputIndex: number, coveredBytecodeBin: Uint8Array, hashtype: number): Promise<Uint8Array>
{
	// NOTE: A signing serialization are the parts of the transaction that should be signed based on a given sighash type.
	//       Note that the input being spent must also be included in this serialization.
	//       For more details, see: https://documentation.cash/protocol/blockchain/transaction/transaction-signing

	// Create a "transaction state", used to extract a lot of the relevant information with Libauth.
	const state = createTransactionContextCommon({
		inputIndex,
		sourceOutput: { satoshis: bigIntToBinUint64LE(BigInt(satoshis)) },
		spendingTransaction: transaction,
	});

	// Generate the signing serialization using mostly information from the generated "transaction state".
	const signingSerialization = generateSigningSerializationBCH({
		correspondingOutput: state.correspondingOutput,
		coveredBytecode: coveredBytecodeBin,
		forkId: CASH_FORK_ID,
		locktime: state.locktime,
		outpointIndex: state.outpointIndex,
		outpointTransactionHash: state.outpointTransactionHash,
		outputValue: state.outputValue,
		sequenceNumber: state.sequenceNumber,
		sha256: await instantiateSha256(),
		signingSerializationType: new Uint8Array([ hashtype ]),
		transactionOutpoints: state.transactionOutpoints,
		transactionOutputs: state.transactionOutputs,
		transactionSequenceNumbers: state.transactionSequenceNumbers,
		version: 2,
	});

	return signingSerialization;
};

/**
* Sign a single transaction input using a private key.
*
* @function
*
* @param transaction        {Transaction} The transaction to use.
* @param satoshis           {number}      The input's satoshi value.
* @param inputIndex         {number}      Input index to sign.
* @param coveredBytecodeBin {Uint8Array}  The input's locking script.
* @param hashtype           {number}      Hash type to use for signing serialization.
* @param privateKeyBin      {Uint8Array}  Private Key in binary format.
*
* @returns {Uint8Array}	The signed transaction input.
*/
export const signTransactionInput = async function(transaction: Transaction, satoshis: number, inputIndex: number, coveredBytecodeBin: Uint8Array, hashtype: number, privateKeyBin: Uint8Array): Promise<Uint8Array>
{
	// Generate the signing serialization for this transaction input.
	const signingSerialization = await createSigningSerialization(transaction, satoshis, inputIndex, coveredBytecodeBin, hashtype);

	// Generate the "sighash" by taking the double SHA256 of the signing serialization.
	const sha256 = await instantiateSha256();
	const sighash = sha256.hash(sha256.hash(signingSerialization));

	// Instantiate the Secp256k1 interface.
	const secp256k1 = await instantiateSecp256k1();

	// Generate a signature over the "sighash" using the passed private key.
	const signatureBin = secp256k1.signMessageHashSchnorr(privateKeyBin, sighash);

	// Append the hashtype to the signature to turn it into a valid transaction signature.
	const transactionSignature = Uint8Array.from([ ...signatureBin, hashtype ]);

	return transactionSignature;
};

/**
* Signs and builds the unlocking script for a P2PKH Input.
*
* @function
*
* @param transaction {Transaction} The transaction being signed.
* @param input       {any}         The input to use.
* @param inputIndex  {number}      The index of the input.
* @param privateKey  {string}      The private key to use.
* @param publicKey   {string}      The public key to use.
* @param address     {string}      The address to use.
*
* @returns {Promise<Input>} The P2PKH output script.
*/
export const unlockP2PKHInput = async function(transaction: Transaction, input: any, inputIndex: number, privateKey: string, publicKey: string, address: string): Promise<Input>
{
	// Extract the bytecode (locking script) from our return address.
	const lockScriptBin = await getLockingBytecodeFromAddress(address);

	// Define SIGHASH_ALL constant.
	const SIGHASH_ALL = 0x41;

	// Generate a transaction signature for this input.
	const signatureBin = await signTransactionInput(transaction, input.satoshis, inputIndex, lockScriptBin, SIGHASH_ALL, hexToBin(privateKey));

	// Build the unlocking script that unlocks the P2PKH locking script.
	const unlockingBytecode = flattenBinArray([ encodeDataPush(signatureBin), encodeDataPush(hexToBin(publicKey)) ]);

	// Add the unlocking script to the input.
	const signedInput = { ...input, unlockingBytecode };

	// Return the signed input.
	return signedInput;
};

/**
* Create a transaction P2PKH output with the given value.
*
* @function
*
* @param address  {string} Bitcoin Cash address to convert to P2PKH lock-code.
* @param satoshis {number} Satoshi value to attach to output
*
* @returns {Promise<Output>} The P2PKH output script.
*/
export const createValueOutput = async function(address: string, satoshis: number): Promise<Output>
{
	// Create the output.
	const valueOutput =
	{
		lockingBytecode: await getLockingBytecodeFromAddress(address),
		satoshis: bigIntToBinUint64LE(BigInt(satoshis)),
	};

	// TODO: We want to do a check here to ensure the satoshi amount is above the dust limit.
	//       However, before we do this, we must refactor our logic in Hop.Cash backend.

	// Return the output.
	return valueOutput;
};

/**
* Create an OP_RETURN output.
*
* @function
*
* @param data        {Uint8Array} OP_RETURN data.
* @param protocolHex {string}     Prefix (in hex) to precede data.
*
* @returns {Promise<Output>} The OP_RETURN output script.
*/
export const createDataOutput = async function(data: Uint8Array, protocolHex: string = ''): Promise<Output>
{
	// Convert opcodes and data to proper types.
	const OP_RETURN = hexToBin('6a');
	const OP_PUSH4 = hexToBin('04');
	const OP_PROTOCOL = hexToBin(protocolHex);

	// Set up an empty data output structure.
	const dataOutput =
	{
		satoshis: bigIntToBinUint64LE(BigInt(0)),
	};

	// Throw an error if a protocol has been provided, but is invalid.
	if(protocolHex && protocolHex.length !== 8)
	{
		throw(new Error(`Cannot create OP_RETURN data output, provided protocol (${protocolHex}, length ${protocolHex.length}) is not 4 bytes of data.`));
	}

	if(protocolHex)
	{
		// Return the data output with the data pushed after the protocol identifier.
		return { ...dataOutput, ...{ lockingBytecode: flattenBinArray([ OP_RETURN, OP_PUSH4, OP_PROTOCOL, encodeDataPush(data) ]) } };
	}

	// Return the data output with the data pushed directly without a protocol identifier.
	return { ...dataOutput, ...{ lockingBytecode: flattenBinArray([ OP_RETURN, encodeDataPush(data) ]) } };
};

/**
* Create a transaction.
*
* @function
*
* @param privateKeyWIF  {string}                     Private Key in WIF format.
* @param unspentOutputs {AddressListUnspentResponse} Prefix (in hex) to precede data.
* @param outputs        {Array<Output>}              Array of outputs to include in transaction.
*
* @returns {Promise<Output>}	The OP_RETURN output script.
*/
export const createTransaction = async function(privateKeyWIF: string, unspentOutputs: AddressListUnspentResponse, outputs: Array<Output>): Promise<Uint8Array>
{
	// Parse the private key wif into the keypair and address.
	const [ privateKey, publicKey, returnAddress ] = await parseWIF(privateKeyWIF);

	// Convert all coins to the Libauth Input format (unsigned)
	const inputs = [ ...unspentOutputs ].map(createUnsignedInput);

	// Assemble the unsigned transaction.
	const transaction =
	{
		version: 2,
		inputs,
		outputs,
		locktime: 0,
	};

	// Sign all inputs and add the generated unlocking scripts to the transaction.
	transaction.inputs = await Promise.all(transaction.inputs.map((input, inputIndex) => unlockP2PKHInput(transaction, input, inputIndex, privateKey, publicKey, returnAddress)));

	// Hex encode the built transaction.
	const encodedTransaction = encodeTransaction(transaction);

	// Return the encoded transaction.
	return encodedTransaction;
};

/**
* Gets the hash of a given transaction.
*
* @function
*
* @param transaction {Uint8Array} The raw transaction.
*
* @returns {Promise<string>} The transaction hash as a hex string.
*/
export const getTransactionHash = async function(transaction: Uint8Array): Promise<string>
{
	// Instantiate LibAuth SHA256 instance.
	const sha256 = await instantiateSha256();

	// Get the Transaction Hash.
	const transactionHash = getTransactionHashLE(sha256, transaction);

	// Convert it to hex.
	const transactionHashInHex = binToHex(transactionHash);

	// Return it.
	return transactionHashInHex;
};
